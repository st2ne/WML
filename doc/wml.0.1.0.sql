/*
Navicat MySQL Data Transfer

Source Server         : local3306
Source Server Version : 50725
Source Host           : localhost:3306
Source Database       : wml-trunk

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2023-04-26 12:57:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `alone_app_version`
-- ----------------------------
DROP TABLE IF EXISTS `alone_app_version`;
CREATE TABLE `alone_app_version` (
  `version` varchar(32) NOT NULL DEFAULT '',
  `update_time` varchar(32) DEFAULT NULL,
  `update_user` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_app_version
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_applog`
-- ----------------------------
DROP TABLE IF EXISTS `alone_applog`;
CREATE TABLE `alone_applog` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `DESCRIBES` varchar(1024) NOT NULL,
  `APPUSER` varchar(32) NOT NULL,
  `LEVELS` varchar(32) DEFAULT NULL,
  `METHOD` varchar(128) DEFAULT NULL,
  `CLASSNAME` varchar(128) DEFAULT NULL,
  `IP` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_9` (`APPUSER`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_applog
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_auth_action`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_action`;
CREATE TABLE `alone_auth_action` (
  `ID` varchar(32) NOT NULL,
  `AUTHKEY` varchar(128) NOT NULL,
  `NAME` varchar(64) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `CTIME` varchar(14) NOT NULL,
  `UTIME` varchar(14) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL,
  `CHECKIS` char(1) NOT NULL,
  `LOGINIS` char(1) NOT NULL COMMENT '默认所有ACTION都要登录',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB alone_action';

-- ----------------------------
-- Records of alone_auth_action
-- ----------------------------
INSERT INTO `alone_auth_action` VALUES ('40288b854a38408e014a384de88a0005', 'action/list', '权限管理_权限定义', null, '20141211154357', '20141211154357', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38b3a6700017', 'actiontree/list', '权限管理_权限构造', null, '20141211173505', '20141211173505', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38b93d0a0022', 'log/list', '系统配置_系统日志', '', '20141211174111', '20150831184926', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38ba24f90024', 'parameter/list', '系统配置_参数定义', '', '20141211174210', '20150831185312', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38bafb830026', 'parameter/editlist', '系统配置_系统参数', '', '20141211174305', '20151016154936', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38bb431b0028', 'parameter/userelist', '系统配置_个性化参数', '', '20141211174324', '20151016154959', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38bd26a0002a', 'dictionary/list', '系统配置_数据字典', '', '20141211174527', '20150831185552', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38beae79002d', 'user/list', '组织用户管理_用户管理', '', '20141211174708', '20150831185712', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38bf10fc002f', 'organization/list', '组织用户管理_组织机构管理', '', '20141211174733', '20150831185753', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402880e68797d437018797d867730004', 'tag/list', '素材库_素材标签管理', null, '20230419124914', '20230419124914', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402880e6879da3a301879da4e76a0001', 'fileresource/list', '素材库_物理资源管理', null, '20230420155042', '20230420155042', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('4028fd127655e8cd017655ea25080000', 'helper/readme', '运维管理_接口API説明', null, '20201212154616', '20201212154616', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a50d918014a50dcfa580005', 'qzTrigger/list', '任务调度_触发器定义', '', '20141216101106', '20171211230511', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a50d918014a50ddb7400007', 'qzTask/list', '任务调度_任务定义', '', '20141216101155', '20171211230536', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a50d918014a50def0aa0009', 'qzScheduler/list', '任务调度_调度实例', null, '20141216101315', '20141216101315', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402880e6879da3a301879da5f5690003', 'filebase/list', '素材库_文件管理', null, '20230420155151', '20230420155151', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402880e687a236a40187a23bf7f4000f', 'categrayfile/list', '素材库_素材管理', null, '20230421131411', '20230421131411', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40289f0e770e500201770e51f5bc0001', 'post/list', '用户管理_岗位管理', null, '20210117110948', '20210117110948', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('2c948a837a7fbd4f017a7fcd2bf10001', 'userrole/list', '事件管理_关系人类型', null, '20210707150950', '20210707150950', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('8a2831b3573c10d0015741b2273c0018', 'user/online', '系统配置_在线用户', null, '20160919170617', '20160919170617', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('2c948a837ce4a6f5017ce4be01ce0001', 'usermsg/list', '消息管理_站内消息检索', null, '20211103154030', '20211103154030', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40289f787af9f5bc017afa0865f60002', 'useraddress/list', '消息管理_通讯录管理', null, '20210731084812', '20210731084812', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402889ac5da5d50f015da5d736060000', 'toolweb/sysbackup', '系统配置_系统备份向导', null, '20170803100531', '20170803100531', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402880e68797d437018797d797ae0002', 'categray/list', '素材库_素材分类管理', null, '20230419124821', '20230419124821', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b8816b352fa8016b3660a1a601cc', 'user/chooseUser', '选择用户', '', '20190608171710', '20190608171710', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '0', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b8816b366111016b3664ebb30002', 'user/query', '查询用户', '', '20190608172152', '20190608172152', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '0', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b8816b366557016b3666da4b002a', 'organization/chooseOrg', '选择组织机构', '', '20190608172358', '20190608172358', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '0', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b8816b366557016b3667b48a002b', 'organization/organizationTree', '查询组织机构树', '', '20190608172454', '20190608172454', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '0', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b8816b366557016b36682d08002c', 'post/choosePost', '选择岗位', '', '20190608172525', '20190608172525', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '0', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b8816b366557016b3668a27c002d', 'post/query', '查询岗位', '', '20190608172555', '20190608172555', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '0', '1');

-- ----------------------------
-- Table structure for `alone_auth_actiontree`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_actiontree`;
CREATE TABLE `alone_auth_actiontree` (
  `ID` varchar(32) NOT NULL,
  `SORT` int(11) NOT NULL,
  `PARENTID` varchar(32) NOT NULL,
  `NAME` varchar(64) NOT NULL,
  `TREECODE` varchar(256) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `TYPE` char(1) NOT NULL COMMENT '分类、菜单、权限',
  `CTIME` varchar(14) NOT NULL,
  `UTIME` varchar(14) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `UUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL,
  `ACTIONID` varchar(32) DEFAULT NULL,
  `DOMAIN` varchar(64) NOT NULL,
  `ICON` varchar(64) DEFAULT NULL,
  `IMGID` varchar(32) DEFAULT NULL,
  `PARAMS` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_7` (`ACTIONID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB; (`action`) REFER `alone/alone_action`';

-- ----------------------------
-- Records of alone_auth_actiontree
-- ----------------------------
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38b93d0a0023', '10', '8a2831b35ac74f63015ae4710d6a005b', '系统日志', '8a2831b35ac74f63015ae4710d6a005b40288b854a38974f014a38b93d0a0023', '', '2', '20141211174111', '20141211174111', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38b93d0a0022', 'alone', 'icon-tip', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38b3a6700018', '2', '40288b854a38408e014a384c4f3c0002', '权限构造', '40288b854a38408e014a384c4f3c000240288b854a38974f014a38b3a6700018', '', '2', '20141211173505', '20141211173505', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38b3a6700017', 'alone', 'icon-category', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38408e014a384c4f3c0002', '98', 'NONE', '系统配置', '40288b854a38408e014a384c4f3c0002', '', '1', '20141211154212', '20210707115739', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', 'alone', 'icon-sprocket_dark', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38bafb830027', '3', '40288b854a38408e014a384c4f3c0002', '参数设置', '40288b854a38408e014a384c4f3c000240288b854a38974f014a38bafb830027', '', '2', '20141211174305', '20151016155834', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38bafb830026', 'alone', 'icon-sprocket_dark', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38bb431b0029', '4', '40288b854a38408e014a384c4f3c0002', '个性化参数', '40288b854a38408e014a384c4f3c000240288b854a38974f014a38bb431b0029', '', '2', '20141211174324', '20141211174435', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38bb431b0028', 'alone', 'icon-client_account_template', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38bd26a0002b', '5', '40288b854a38408e014a384c4f3c0002', '数据字典', '40288b854a38408e014a384c4f3c000240288b854a38974f014a38bd26a0002b', '', '2', '20141211174527', '20141211174527', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38bd26a0002a', 'alone', 'icon-address-book', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38be1805002c', '2', 'NONE', '用户管理', '40288b854a38974f014a38be1805002c', '', '1', '20141211174629', '20230419133730', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', 'alone', 'icon-group_green_edit', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38beae79002e', '1', '40288b854a38974f014a38be1805002c', '用户管理', '40288b854a38974f014a38be1805002c40288b854a38974f014a38beae79002e', '', '2', '20141211174708', '20141211174708', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38beae79002d', 'alone', 'icon-hire-me', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38bf10fc0030', '2', '40288b854a38974f014a38be1805002c', '组织管理', '40288b854a38974f014a38be1805002c40288b854a38974f014a38bf10fc0030', '', '2', '20141211174733', '20170319103800', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38bf10fc002f', 'alone', 'icon-customers', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402880e68797d437018797d867740005', '2', '402880e68797d437018797d65e6c0001', '素材标签管理', '402880e68797d437018797d65e6c0001402880e68797d437018797d867740005', '', '2', '20230419124914', '20230419124914', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402880e68797d437018797d867730004', 'alone', 'icon-tag', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402880e6879da3a301879da4e76b0002', '3', '402880e68797d437018797d65e6c0001', '物理资源管理', '402880e68797d437018797d65e6c0001402880e6879da3a301879da4e76b0002', '', '2', '20230420155042', '20230420155042', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402880e6879da3a301879da4e76a0001', 'alone', 'icon-database', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402888ac506eb41b01506fa9b0a30027', '1', '40288b854a38408e014a384c4f3c0002', '注册参数', '40288b854a38408e014a384c4f3c0002402888ac506eb41b01506fa9b0a30027', '', '2', '20151016160003', '20151016160003', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38ba24f90024', 'alone', 'icon-edit', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a50d918014a50dc82980004', '11', '40288b854a38408e014a384c4f3c0002', '任务调度', '40288b854a38408e014a384c4f3c000240288b854a50d918014a50dc82980004', '', '1', '20141216101036', '20141216101036', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, 'alone', 'icon-application_osx_terminal', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a50d918014a50def0aa000a', '3', '40288b854a50d918014a50dc82980004', '调度实例', '40288b854a38408e014a384c4f3c000240288b854a50d918014a50dc8298000440288b854a50d918014a50def0aa000a', '', '2', '20141216101315', '20141216101315', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a50d918014a50def0aa0009', 'alone', 'icon-future-projects', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402888a84f835c36014f835f72cb000e', '1', '40288b854a38408e014a384c4f3c0002', '权限定义', '40288b854a38408e014a384c4f3c0002402888a84f835c36014f835f72cb000e', '', '2', '20150831184834', '20150831184834', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38408e014a384de88a0005', 'alone', 'icon-communication', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402880e6879da3a301879da5f5690004', '4', '402880e68797d437018797d65e6c0001', '文件管理', '402880e68797d437018797d65e6c0001402880e6879da3a301879da5f5690004', '', '2', '20230420155151', '20230420155151', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402880e6879da3a301879da5f5690003', 'alone', 'icon-documents_email', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('8a2831b3573c10d0015741b2273c0019', '6', '8a2831b35ac74f63015ae4710d6a005b', '在线用户', '8a2831b35ac74f63015ae4710d6a005b8a2831b3573c10d0015741b2273c0019', '', '2', '20160919170617', '20160919170617', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '8a2831b3573c10d0015741b2273c0018', 'alone', 'icon-group_green_new', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('8a2831b35ac74f63015ae4710d6a005b', '99', 'NONE', '运维管理', '8a2831b35ac74f63015ae4710d6a005b', '', '1', '20170319104138', '20210707115728', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', 'alone', 'icon-showreel', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402889ac5da5d50f015da5d736110001', '15', '8a2831b35ac74f63015ae4710d6a005b', '系统备份向导', '8a2831b35ac74f63015ae4710d6a005b402889ac5da5d50f015da5d736110001', '', '2', '20170803100531', '20170803100623', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402889ac5da5d50f015da5d736060000', 'alone', 'icon-save', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402880e687a236a40187a23bf7f50010', '5', '402880e68797d437018797d65e6c0001', '素材管理', '402880e68797d437018797d65e6c0001402880e687a236a40187a23bf7f50010', '', '2', '20230421131411', '20230421131411', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402880e687a236a40187a23bf7f4000f', 'alone', 'icon-premium', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402880ef6046149d0160461a366b0001', '1', '40288b854a50d918014a50dc82980004', '任务定义', '40288b854a38408e014a384c4f3c000240288b854a50d918014a50dc82980004402880ef6046149d0160461a366b0001', '', '2', '20171211230331', '20171211230331', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a50d918014a50ddb7400007', 'alone', 'icon-business-contact', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402880ef6046149d0160461aac480002', '2', '40288b854a50d918014a50dc82980004', '触发器定义', '40288b854a38408e014a384c4f3c000240288b854a50d918014a50dc82980004402880ef6046149d0160461aac480002', '', '2', '20171211230401', '20171211230401', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a50d918014a50dcfa580005', 'alone', 'icon-full-time', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('4028fd127655e8cd017655ea25130001', '1', '8a2831b35ac74f63015ae4710d6a005b', '接口API説明', '8a2831b35ac74f63015ae4710d6a005b4028fd127655e8cd017655ea25130001', '', '2', '20201212154616', '20201221102157', '40288b854a329988014a329a12f30002', '2c909b2b602c54c801602c82c46f0027', '1', '4028fd127655e8cd017655ea25080000', 'alone', 'icon-networking', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402880e68797d437018797d65e6c0001', '1', 'NONE', '素材库', '402880e68797d437018797d65e6c0001', '', '1', '20230419124701', '20230419124701', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, 'alone', 'icon-delicious', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402880e68797d437018797d797af0003', '1', '402880e68797d437018797d65e6c0001', '素材分类管理', '402880e68797d437018797d65e6c0001402880e68797d437018797d797af0003', '', '2', '20230419124821', '20230419124921', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402880e68797d437018797d797ae0002', 'alone', 'icon-sitemap', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40289f0e770e500201770e51f5bd0002', '3', '40288b854a38974f014a38be1805002c', '角色管理', '40288b854a38974f014a38be1805002c40289f0e770e500201770e51f5bd0002', '', '2', '20210117110948', '20210117162035', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40289f0e770e500201770e51f5bc0001', 'alone', 'icon-special-offer', null, '');

-- ----------------------------
-- Table structure for `alone_auth_organization`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_organization`;
CREATE TABLE `alone_auth_organization` (
  `ID` varchar(32) NOT NULL,
  `TREECODE` varchar(256) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `NAME` varchar(64) NOT NULL,
  `CTIME` varchar(14) NOT NULL,
  `UTIME` varchar(14) NOT NULL,
  `STATE` char(1) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `SORT` int(11) DEFAULT NULL,
  `TYPE` char(1) NOT NULL COMMENT '组织类型：1科室、2班组、3队组、0其他',
  `APPID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='组织类型：科室、班组、队组、其他';

-- ----------------------------
-- Records of alone_auth_organization
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_auth_pop`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_pop`;
CREATE TABLE `alone_auth_pop` (
  `ID` varchar(32) NOT NULL,
  `POPTYPE` varchar(1) NOT NULL COMMENT '1人、2组织机构、3岗位',
  `OID` varchar(32) NOT NULL COMMENT '人ID、组织机构ID、岗位ID',
  `ONAME` varchar(128) NOT NULL COMMENT '人NAME、组织机构NAME、岗位NAME',
  `TARGETTYPE` varchar(64) NOT NULL COMMENT '权限业务类型',
  `TARGETID` varchar(32) NOT NULL COMMENT '权限业务ID',
  `TARGETNAME` varchar(128) DEFAULT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_auth_pop
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_auth_post`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_post`;
CREATE TABLE `alone_auth_post` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `ORGANIZATIONID` varchar(32) DEFAULT NULL,
  `NAME` varchar(64) NOT NULL,
  `EXTENDIS` varchar(2) NOT NULL COMMENT '0:否1:是（默认否）',
  `APPID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_51` (`ORGANIZATIONID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_auth_post
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_auth_postaction`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_postaction`;
CREATE TABLE `alone_auth_postaction` (
  `ID` varchar(32) NOT NULL,
  `MENUID` varchar(32) NOT NULL,
  `POSTID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_8` (`MENUID`),
  KEY `FK_Reference_9` (`POSTID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB; (`actionid`) REFER `alone/alone_actio';

-- ----------------------------
-- Records of alone_auth_postaction
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_auth_user`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_user`;
CREATE TABLE `alone_auth_user` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(64) NOT NULL,
  `PASSWORD` varchar(32) NOT NULL COMMENT 'MD5(password+loginname)',
  `COMMENTS` varchar(128) DEFAULT NULL,
  `TYPE` char(1) DEFAULT NULL COMMENT '1:系统用户:2其他3超级用户',
  `CTIME` varchar(14) NOT NULL,
  `UTIME` varchar(14) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL,
  `LOGINNAME` varchar(64) NOT NULL,
  `LOGINTIME` varchar(14) DEFAULT NULL,
  `IMGID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB alone_user';

-- ----------------------------
-- Records of alone_auth_user
-- ----------------------------
INSERT INTO `alone_auth_user` VALUES ('40288b854a329988014a329a12f30002', '系统管理员', '6C832EDB4410D94088918F240949593F', '', '3', '20141210130925', '20220704125920', 'userId', '40288b854a329988014a329a12f30002', '1', 'sysadmin', '20230426122656', '402880e687b2a0af0187b2a4a5630001');
INSERT INTO `alone_auth_user` VALUES ('402880e68791ffcc018792064a170001', 'apiuser', 'D593292F60361316D8BDF7997262E814', '', '9', '20230418094138', '20230418094138', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', 'apiuser', null, null);
INSERT INTO `alone_auth_user` VALUES ('402880e687b251140187b256409b0019', 'guest', 'B9BCAE8EE035EC01F8D6A1B12926F153', '', '1', '20230424161649', '20230424161649', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', 'guest', '20230426114931', null);

-- ----------------------------
-- Table structure for `alone_auth_userorg`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_userorg`;
CREATE TABLE `alone_auth_userorg` (
  `ID` varchar(32) NOT NULL,
  `USERID` varchar(32) NOT NULL,
  `ORGANIZATIONID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `DOC_USERID` (`USERID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB; (`organizationid`) REFER `alone/alone';

-- ----------------------------
-- Records of alone_auth_userorg
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_auth_userpost`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_userpost`;
CREATE TABLE `alone_auth_userpost` (
  `ID` varchar(32) NOT NULL,
  `USERID` varchar(32) NOT NULL,
  `POSTID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_auth_userpost
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_dictionary_entity`
-- ----------------------------
DROP TABLE IF EXISTS `alone_dictionary_entity`;
CREATE TABLE `alone_dictionary_entity` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(12) NOT NULL,
  `UTIME` varchar(12) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL DEFAULT '',
  `NAME` varchar(128) NOT NULL,
  `ENTITYINDEX` varchar(128) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `TYPE` char(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB';

-- ----------------------------
-- Records of alone_dictionary_entity
-- ----------------------------
INSERT INTO `alone_dictionary_entity` VALUES ('174c8dacc6004734b7c9af22a0990a27', '202011252017', '202011252017', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '0', '13111058226', 'demo-FE01C', '{none}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('2c902a8d6dcf9730016e031a58762b86', '201910252128', '201910252128', '2c902a8d6dcf9730016e0317f7162b80', '2c902a8d6dcf9730016e0317f7162b80', '1', '用户可选头像', 'USER_DEFAULT_PHOTOS', '{默认头像1:2c902a8d6dcf9730016e031a7ce62b87, 默认头像2:2c902a8d6dcf9730016e031ba00a2b89, 默认头像3:2c902a8d6dcf9...', '2');
INSERT INTO `alone_dictionary_entity` VALUES ('402894ca49600f600149602141820000', '201410301617', '201410301617', '12312323123123', '12312323123123', '1', '系统菜单域', 'ALONE_MENU_DOMAIN', '{前台:HOME}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('40289f0e7683108c0176831b81200000', '202012211023', '202012211023', '2c909b2b602c54c801602c82c46f0027', '2c909b2b602c54c801602c82c46f0027', '1', '随机登陆用户类型', 'USER_RANDOM_LOGINTYPE', '{普通用户:1, 超级用户:3}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('6578fd8259db44b8ad7e711f7ec78b17', '201905052103', '201905052103', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '0', 'niHH8X7aa8Hns5ZUIlJ6Xh5Cxuc+BbqE', '9zmyZ8IsSXs=', '{none}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('7263206837704f3389782fdddf33d550', '201811122059', '201811122059', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '0', 'niHH8X7aa8Hns5ZUIlJ6Xh5Cxuc+BbqE', 'dLRC0l1Fl/Q=', '{none}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('9c157c0818ef4784aa620eb3854d0f17', '201810161509', '201810161509', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '0', 'rYGht6RSk5hudeZx88NZ/907OP6ufvcY', '+nQHCpAShLw=', '{none}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('f1c8290d909f4ab5beeb5455e9c76ec1', '201902091940', '201902091940', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '0', 'niHH8X7aa8Hns5ZUIlJ6Xh5Cxuc+BbqE', 'JXAVJSQ//As=', '{none}', '1');

-- ----------------------------
-- Table structure for `alone_dictionary_type`
-- ----------------------------
DROP TABLE IF EXISTS `alone_dictionary_type`;
CREATE TABLE `alone_dictionary_type` (
  `CTIME` varchar(12) NOT NULL,
  `UTIME` varchar(12) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `ENTITYTYPE` varchar(128) NOT NULL,
  `ENTITY` varchar(32) NOT NULL,
  `ID` varchar(32) NOT NULL DEFAULT '',
  `SORT` int(11) NOT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `TREECODE` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_8` (`ENTITY`) USING BTREE,
  CONSTRAINT `alone_dictionary_type_ibfk_1` FOREIGN KEY (`ENTITY`) REFERENCES `alone_dictionary_entity` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB alone_dictionary_type';

-- ----------------------------
-- Records of alone_dictionary_type
-- ----------------------------
INSERT INTO `alone_dictionary_type` VALUES ('201910252128', '201910252128', '2c902a8d6dcf9730016e0317f7162b80', '2c902a8d6dcf9730016e0317f7162b80', '1', '默认头像1', '', '2c902a8d6dcf9730016e031a7ce62b87', '2c902a8d6dcf9730016e031a58762b86', '2c902a8d6dcf9730016e031aa7cf2b88', '1', 'NONE', '2c902a8d6dcf9730016e031aa7cf2b88');
INSERT INTO `alone_dictionary_type` VALUES ('201910252129', '201910252129', '2c902a8d6dcf9730016e0317f7162b80', '2c902a8d6dcf9730016e0317f7162b80', '1', '默认头像2', '', '2c902a8d6dcf9730016e031ba00a2b89', '2c902a8d6dcf9730016e031a58762b86', '2c902a8d6dcf9730016e031bb7892b8a', '2', 'NONE', '2c902a8d6dcf9730016e031bb7892b8a');
INSERT INTO `alone_dictionary_type` VALUES ('201910252129', '201910252129', '2c902a8d6dcf9730016e0317f7162b80', '2c902a8d6dcf9730016e0317f7162b80', '1', '默认头像3', '', '2c902a8d6dcf9730016e031bd7172b8b', '2c902a8d6dcf9730016e031a58762b86', '2c902a8d6dcf9730016e031bf85b2b8d', '3', 'NONE', '2c902a8d6dcf9730016e031bf85b2b8d');
INSERT INTO `alone_dictionary_type` VALUES ('201410301617', '201410301617', '12312323123123', '12312323123123', '1', '前台', '', 'HOME', '402894ca49600f600149602141820000', '402894ca49600f6001496021a7020001', '1', 'NONE', '402894ca49600f6001496021a7020001');
INSERT INTO `alone_dictionary_type` VALUES ('202012211023', '202012211023', '2c909b2b602c54c801602c82c46f0027', '2c909b2b602c54c801602c82c46f0027', '1', '普通用户', '', '1', '40289f0e7683108c0176831b81200000', '40289f0e7683108c0176831c2a480001', '1', 'NONE', '40289f0e7683108c0176831c2a480001');
INSERT INTO `alone_dictionary_type` VALUES ('202012211024', '202012211024', '2c909b2b602c54c801602c82c46f0027', '2c909b2b602c54c801602c82c46f0027', '1', '超级用户', '', '3', '40289f0e7683108c0176831b81200000', '40289f0e7683108c0176831c567f0002', '3', 'NONE', '40289f0e7683108c0176831c567f0002');

-- ----------------------------
-- Table structure for `alone_parameter`
-- ----------------------------
DROP TABLE IF EXISTS `alone_parameter`;
CREATE TABLE `alone_parameter` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(12) NOT NULL,
  `UTIME` varchar(12) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  `STATE` char(1) NOT NULL,
  `PKEY` varchar(64) NOT NULL,
  `PVALUE` varchar(2048) NOT NULL,
  `RULES` varchar(256) DEFAULT NULL,
  `DOMAIN` varchar(64) DEFAULT NULL,
  `COMMENTS` varchar(256) DEFAULT NULL,
  `VTYPE` char(1) NOT NULL COMMENT ' 文本：1 枚举：2',
  `USERABLE` varchar(1) NOT NULL COMMENT '0否，1是',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB; InnoDB free: 12288 kB alone_parameter';

-- ----------------------------
-- Records of alone_parameter
-- ----------------------------
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3112800ed', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.useredit.reception.able', '1', 'config.useredit.reception.able', 'false', '', '用户权限', '前台是否可以修改当前用户信息', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3113e00ee', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.firstBind.message', '1', 'config.sys.firstBind.message', '欢迎使用本系统!', '', '登录', '用户首次通过外部账号绑定到系统登录后收到的提示信息', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3114100ef', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.firstlogin.message', '1', 'config.sys.firstlogin.message', '欢迎使用本系统，如果您的密码是系统自动生成的请及时重置密码!', '', '登录', '用户首次正常登录时的提示信息', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310e000d1', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.upload.types', '1', 'config.doc.upload.types', 'png,jpg,jpeg,gif,zip,doc,docx,xls,xlsx,pdf,ppt,pptx,web,rar,txt,flv,mp3,mp4,dcr,md,whtm', '', '文件', '上传文件允许的后缀名、文件类型', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310e200d2', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.img.thumbnails.width', '1', 'config.doc.img.thumbnails.width', '100,200,300,400,500,600,700,800,900,1000', '', '文件', '允许的图片压缩宽度', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310e400d3', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.perfect.userinfo.able', '1', 'config.sys.perfect.userinfo.able', 'false', '', '注册', '用户登录后是否必须先完善个人信息,true时，如果用户没有完善个人信息直接跳到个人信息编辑页面(isCompleteUserInfo(LoginUser currentUser)方法判断用户信息是否完善)', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3114600f1', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.media.upload.types', '1', 'config.doc.media.upload.types', 'mp3,mp4', '', '文件', '上传多媒体允许的后缀名、文件类型', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3114800f2', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.img.upload.length.max', '1', 'config.doc.img.upload.length.max', '10485760', '', '文件', '上传图片允许文件大小,单位是字节(1048576:1m)', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3114b00f3', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.login.default.url', '1', 'config.login.default.url', 'default', '', '登录', '默认的登陆页面,可选项:default[默认内部登陆窗口],/ldap/PubLogin.html[ldap登陆窗口]', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3114e00f4', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.restful.secret.key', '1', 'config.restful.secret.key', 'F59BD65F7EDAFB087A81D4DCA06C4910', '', 'restful接口', '系统通用的接口密码', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3114400f0', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.verifycode.able', '1', 'config.sys.verifycode.able', 'true', '', '登录', '是否启用用户登录验证码', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3111500e6', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.link.newwindow.target', '1', 'config.sys.link.newwindow.target', '_blank', '', '文字标记/通用配置', '是否允许系统在新窗口打开页面_self（在本窗口打开页面）/_blank（在新窗口打开页面）', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3111800e7', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.restful.whitelist.ips', '1', 'config.restful.whitelist.ips', '127.0.0.1,192.*.*.*', '', 'restful接口', '允许访问的白名单，多个ip间用逗号分隔支持通配符192.168.1.1,192.168.*.2,192.168.3.17-192.168.3.38', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3111c00e8', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.upload.length.max', '1', 'config.doc.upload.length.max', '1048576000', '', '文件', '上传文件允许文件大小,单位是字节(2147483648:2046m,10485760:10m)', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3111d00e9', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.none.img.path', '1', 'config.doc.none.img.path', 'text/img/none.png', '', '文件', '如果图片文件不存在时，默认的图片', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3112000ea', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.password.loginfail.loginname.maxnum', '1', 'config.sys.password.loginfail.loginname.maxnum', '5', '', '密码', '允许登陆名最大登陆失败次数，0不限制', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3112300eb', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.restful.state', '1', 'config.restful.state', 'true', '', 'restful接口', '是否启用restful接口', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3112600ec', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.foot', '1', 'config.sys.foot', 'WML素材库', '', '文字标记/通用配置', '系统页面最下方显示', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310e700d4', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.img.upload.types', '1', 'config.doc.img.upload.types', 'png,jpg,jpeg,gif', '', '文件', '上传图片允许的后缀名、文件类型', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310ea00d5', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.link.docview.target', '1', 'config.sys.link.docview.target', '_blank', '', '文字标记/通用配置', '附件预览时是否在新窗口打开预览页面auto(自动)/_blank(强制新窗口)', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310ec00d6', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.password.update.regex', '1', 'config.sys.password.update.regex', '(.*)', '', '密码', '当前用户前台修改密码时,密码验证-正则表达式,如:密码规则为字母加数字至少 6位:^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,32}$,如:密码不包含换行符:(.*)', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310ee00d7', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.user.mng.hide.super', '1', 'config.user.mng.hide.super', 'false', '', '用户权限', '用户管理是否默认隐藏管理员和接口用户', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310f000d8', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.restful.debug', '1', 'config.restful.debug', 'false', '', 'restful接口', '是否启用接口调试模式', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310f200d9', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.verifycode.checknum', '1', 'config.sys.verifycode.checknum', '3', '', '登录', '用户免验证码登录失败次数，超过该数量就启用验证码，为0时总是启用验证码，外部免登绑定账户时直接启用验证码', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310f500da', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.useredit.showName', '1', 'config.useredit.showName', 'false', '', '用户权限', '前台编辑当前用户信息时，是否可以修改用户名', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310f700db', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.enforce.password.update', '1', 'config.sys.enforce.password.update', 'false', '', '密码', '是否强制当前用户修改默认密码(true强制修改),在当前用户密码和默认密码一致时（没有修改过默认密码）。', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310fa00dc', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.password.provider.type', '1', 'config.password.provider.type', 'SAFE2', '', '密码', 'SAFE2：前端加随机盐', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310fc00dd', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.none.photo.path', '1', 'config.doc.none.photo.path', 'text/img/photo.png', '', '文件', '如果头像文件不存在时，默认的图片', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310ff00de', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.show.local.regist.able', '1', 'config.show.local.regist.able', 'false', '', '注册', '是否允许本地注册新用户', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3110100df', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.restful.secret.type', '1', 'config.restful.secret.type', 'complex', '', 'restful接口', '权限校验模式complex[使用安全码的同时必须带入操作人信息]|simple[简单模式直接使用安全码访问]|none[不校验验证码，只校验ip白名单]', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3110300e0', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.restful.whitelist.state', '1', 'config.restful.whitelist.state', 'true', '', 'restful接口', '是否启用ip白名单', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3110500e1', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.password.loginfail.ip.maxnum', '1', 'config.sys.password.loginfail.ip.maxnum', '5', '', '密码', '允许IP最大登陆失败次数，0不限制', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3110800e2', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.basepath.full.able', '1', 'config.sys.basepath.full.able', 'true', '', '文字标记/通用配置', '是否使用绝对路径的basepath', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3110b00e3', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.registed.audit', '1', 'config.registed.audit', 'false', '', '注册', '用户前台注册后是否为待审核状态,true为需要审核,false直接为可用状态', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3110f00e4', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.logout.plus.url', '1', 'config.logout.plus.url', 'NONE', '', '登录', '外部注销地址，本地注銷完成后將跳轉到此地址,可选项:NONE[无外部注销地址],http://127.0.0.1:8080/cas/login/logout.do', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc3111200e5', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.login.noauth.able', '1', 'config.login.noauth.able', 'true', '', '登录', '是否允许免密登陆本系统：true|false', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310a800cd', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.title', '1', 'config.sys.title', 'WML', '', '文字标记/通用配置', '系统标题', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310d900ce', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.url.free.path.prefix', '1', 'config.url.free.path.prefix', 'Pub', '', '登录', '是否允许用户不登录就访问知识页面，可选项：NONEPAGE(不允许访问)Pub(允许访问)', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310dc00cf', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.default.password', '1', 'config.default.password', '111111', '', '密码', '用户创建时的默认密码', '1', '0');
INSERT INTO `alone_parameter` VALUES ('402880e687bb45480187bbc310de00d0', '202304261212', '202304261212', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.password.update.tip', '1', 'config.sys.password.update.tip', '密码不能包含换行符', '', '密码', '当前用户前台修改密码时,密码验证-正则表达式验证失败提示信息', '1', '0');

-- ----------------------------
-- Table structure for `alone_parameter_local`
-- ----------------------------
DROP TABLE IF EXISTS `alone_parameter_local`;
CREATE TABLE `alone_parameter_local` (
  `ID` varchar(32) NOT NULL,
  `PARAMETERID` varchar(32) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PVALUE` varchar(2048) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_50` (`PARAMETERID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_parameter_local
-- ----------------------------

-- ----------------------------
-- Table structure for `farm_qz_scheduler`
-- ----------------------------
DROP TABLE IF EXISTS `farm_qz_scheduler`;
CREATE TABLE `farm_qz_scheduler` (
  `ID` varchar(32) NOT NULL,
  `AUTOIS` varchar(2) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `TASKID` varchar(32) NOT NULL,
  `TRIGGERID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_18` (`TASKID`),
  KEY `FK_Reference_19` (`TRIGGERID`),
  CONSTRAINT `FK_Reference_18` FOREIGN KEY (`TASKID`) REFERENCES `farm_qz_task` (`ID`),
  CONSTRAINT `FK_Reference_19` FOREIGN KEY (`TRIGGERID`) REFERENCES `farm_qz_trigger` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_qz_scheduler
-- ----------------------------

-- ----------------------------
-- Table structure for `farm_qz_task`
-- ----------------------------
DROP TABLE IF EXISTS `farm_qz_task`;
CREATE TABLE `farm_qz_task` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `JOBCLASS` varchar(128) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  `JOBPARAS` varchar(1024) DEFAULT NULL,
  `JOBKEY` varchar(128) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_qz_task
-- ----------------------------

-- ----------------------------
-- Table structure for `farm_qz_trigger`
-- ----------------------------
DROP TABLE IF EXISTS `farm_qz_trigger`;
CREATE TABLE `farm_qz_trigger` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `DESCRIPT` varchar(128) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_qz_trigger
-- ----------------------------
INSERT INTO `farm_qz_trigger` VALUES ('2c948a837cb63d57017cb66fcf27000d', '20211025155233', '20211025155233', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '每年每月每日3时1分1秒', '1 1 3 * * ? *', '每日凌晨执行');

-- ----------------------------
-- Table structure for `wml_c_categray`
-- ----------------------------
DROP TABLE IF EXISTS `wml_c_categray`;
CREATE TABLE `wml_c_categray` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(512) DEFAULT NULL,
  `SORT` int(11) NOT NULL,
  `PARENTID` varchar(32) NOT NULL,
  `NAME` varchar(32) NOT NULL,
  `TREECODE` varchar(256) NOT NULL,
  `TYPE` varchar(2) NOT NULL,
  `CSORT` varchar(32) NOT NULL,
  `CLAYOUT` varchar(32) NOT NULL,
  `CMODEL` varchar(32) NOT NULL,
  `CNUM` int(11) NOT NULL,
  `ALLNUM` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wml_c_categray
-- ----------------------------
INSERT INTO `wml_c_categray` VALUES ('402880e687bbd0360187bbea2c650011', '20230426125458', '20230426125458', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '1', 'NONE', '图片素材', '402880e687bbd0360187bbea2c650011', '0', 'NONE', 'NONE', 'NONE', '0', '0');
INSERT INTO `wml_c_categray` VALUES ('402880e687bbd0360187bbea7e510012', '20230426125519', '20230426125519', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '1', '402880e687bbd0360187bbea2c650011', '人物', '402880e687bbd0360187bbea2c650011402880e687bbd0360187bbea7e510012', '0', 'NONE', 'NONE', 'NONE', '0', '0');
INSERT INTO `wml_c_categray` VALUES ('402880e687bbd0360187bbea7f530013', '20230426125520', '20230426125520', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '2', '402880e687bbd0360187bbea2c650011', '风景', '402880e687bbd0360187bbea2c650011402880e687bbd0360187bbea7f530013', '0', 'NONE', 'NONE', 'NONE', '0', '0');
INSERT INTO `wml_c_categray` VALUES ('402880e687bbd0360187bbea7ff10014', '20230426125520', '20230426125520', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '3', '402880e687bbd0360187bbea2c650011', '动物', '402880e687bbd0360187bbea2c650011402880e687bbd0360187bbea7ff10014', '0', 'NONE', 'NONE', 'NONE', '0', '0');
INSERT INTO `wml_c_categray` VALUES ('402880e687bbd0360187bbeaaa7a0015', '20230426125531', '20230426125531', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '2', 'NONE', '媒体素材', '402880e687bbd0360187bbeaaa7a0015', '0', 'NONE', 'NONE', 'NONE', '0', '0');
INSERT INTO `wml_c_categray` VALUES ('402880e687bbd0360187bbead5790016', '20230426125542', '20230426125542', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '3', 'NONE', '其它文件', '402880e687bbd0360187bbead5790016', '0', 'NONE', 'NONE', 'NONE', '0', '0');

-- ----------------------------
-- Table structure for `wml_c_categrayfile`
-- ----------------------------
DROP TABLE IF EXISTS `wml_c_categrayfile`;
CREATE TABLE `wml_c_categrayfile` (
  `ID` varchar(32) NOT NULL,
  `FILEID` varchar(32) NOT NULL,
  `CATEGRAYID` varchar(32) NOT NULL,
  `PSTATE` varchar(2) DEFAULT NULL,
  `DUSER` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wml_c_categrayfile
-- ----------------------------

-- ----------------------------
-- Table structure for `wml_c_categraytag`
-- ----------------------------
DROP TABLE IF EXISTS `wml_c_categraytag`;
CREATE TABLE `wml_c_categraytag` (
  `ID` varchar(32) NOT NULL,
  `CATEGRAYID` varchar(32) NOT NULL,
  `TAGID` varchar(32) NOT NULL,
  `CNUM` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_WML_C_CA_REFERENCE_WML_C_CA` (`CATEGRAYID`),
  KEY `FK_WML_C_CA_REFERENCE_WML_C_TA` (`TAGID`),
  CONSTRAINT `FK_WML_C_CA_REFERENCE_WML_C_CA` FOREIGN KEY (`CATEGRAYID`) REFERENCES `wml_c_categray` (`ID`),
  CONSTRAINT `FK_WML_C_CA_REFERENCE_WML_C_TA` FOREIGN KEY (`TAGID`) REFERENCES `wml_c_tag` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wml_c_categraytag
-- ----------------------------
INSERT INTO `wml_c_categraytag` VALUES ('402880e687bbd0360187bbeb88da001a', '402880e687bbd0360187bbea2c650011', '402880e687bbd0360187bbeb20890017', '0');
INSERT INTO `wml_c_categraytag` VALUES ('402880e687bbd0360187bbeb8967001b', '402880e687bbd0360187bbea2c650011', '402880e687bbd0360187bbeb4df90018', '0');
INSERT INTO `wml_c_categraytag` VALUES ('402880e687bbd0360187bbeb89bb001c', '402880e687bbd0360187bbea2c650011', '402880e687bbd0360187bbeb77090019', '0');

-- ----------------------------
-- Table structure for `wml_c_tag`
-- ----------------------------
DROP TABLE IF EXISTS `wml_c_tag`;
CREATE TABLE `wml_c_tag` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `PSTATE` varchar(1) NOT NULL,
  `NAME` varchar(32) NOT NULL,
  `GROUPNAME` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wml_c_tag
-- ----------------------------
INSERT INTO `wml_c_tag` VALUES ('402880e687bbd0360187bbeb20890017', '20230426125601', '系统管理员', '', '1', '红', '');
INSERT INTO `wml_c_tag` VALUES ('402880e687bbd0360187bbeb4df90018', '20230426125613', '系统管理员', '', '1', '黄', '');
INSERT INTO `wml_c_tag` VALUES ('402880e687bbd0360187bbeb77090019', '20230426125623', '系统管理员', '', '1', '蓝', '');

-- ----------------------------
-- Table structure for `wml_f_file`
-- ----------------------------
DROP TABLE IF EXISTS `wml_f_file`;
CREATE TABLE `wml_f_file` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `PSTATE` varchar(1) NOT NULL,
  `EXNAME` varchar(32) NOT NULL,
  `RELATIVEPATH` varchar(1024) NOT NULL,
  `SECRET` varchar(32) NOT NULL,
  `FILENAME` varchar(128) NOT NULL,
  `TITLE` varchar(256) NOT NULL,
  `FILESIZE` int(11) NOT NULL,
  `SYSNAME` varchar(128) NOT NULL,
  `RESOURCEID` varchar(32) NOT NULL,
  `APPID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_WML_F_FI_RE_WDAP_F_WML_F_RE` (`RESOURCEID`),
  CONSTRAINT `FK_WML_F_FI_RE_WDAP_F_WML_F_RE` FOREIGN KEY (`RESOURCEID`) REFERENCES `wml_f_resource` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wml_f_file
-- ----------------------------

-- ----------------------------
-- Table structure for `wml_f_resource`
-- ----------------------------
DROP TABLE IF EXISTS `wml_f_resource`;
CREATE TABLE `wml_f_resource` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `TITLE` varchar(128) NOT NULL,
  `PATH` varchar(1024) NOT NULL,
  `STATE` varchar(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wml_f_resource
-- ----------------------------
INSERT INTO `wml_f_resource` VALUES ('402880e6879dbb7501879dc088e40001', '20230420162053', '20230420162053', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '', '默认库', 'INIT', '1');

-- ----------------------------
-- Table structure for `wml_f_text`
-- ----------------------------
DROP TABLE IF EXISTS `wml_f_text`;
CREATE TABLE `wml_f_text` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `FILETEXT` longtext NOT NULL,
  `FILEID` varchar(32) NOT NULL,
  `COMPLETEIS` varchar(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_WML_F_TE_RE_WDAP_T_WML_F_FI` (`FILEID`),
  CONSTRAINT `FK_WML_F_TE_RE_WDAP_T_WML_F_FI` FOREIGN KEY (`FILEID`) REFERENCES `wml_f_file` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wml_f_text
-- ----------------------------

-- ----------------------------
-- Table structure for `wml_f_visit`
-- ----------------------------
DROP TABLE IF EXISTS `wml_f_visit`;
CREATE TABLE `wml_f_visit` (
  `ID` varchar(32) NOT NULL,
  `APPID` varchar(32) NOT NULL,
  `DOWNUM` int(11) NOT NULL,
  `VIEWNUM` int(11) NOT NULL,
  `VISIT` int(11) DEFAULT NULL,
  `PRAISE` int(11) DEFAULT NULL,
  `COMMENTS` int(11) DEFAULT NULL,
  `FAVORITES` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wml_f_visit
-- ----------------------------

-- ----------------------------
-- Table structure for `wml_f_visitlogs`
-- ----------------------------
DROP TABLE IF EXISTS `wml_f_visitlogs`;
CREATE TABLE `wml_f_visitlogs` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(14) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `APPID` varchar(32) NOT NULL,
  `TYPE` varchar(2) NOT NULL,
  `INTFLAG` int(11) DEFAULT NULL,
  `STRFLAG` varchar(256) DEFAULT NULL,
  `CUSERNAME` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wml_f_visitlogs
-- ----------------------------
