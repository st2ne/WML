package com.farm.material.service;

import com.farm.material.domain.Categrayfile;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.auth.domain.LoginUser;


public interface CategrayfileServiceInter {
	
	public Categrayfile insertCategrayfileEntity(Categrayfile entity, LoginUser user);

	
	public Categrayfile editCategrayfileEntity(Categrayfile entity, LoginUser user);

	
	public void deleteCategrayfileEntity(String id, LoginUser user);

	
	public Categrayfile getCategrayfileEntity(String id);

	
	public DataQuery createCategrayfileSimpleQuery(DataQuery query);

	
	public void delByLogic(String categrayFileId, LoginUser user);

	
	public void restoreFile(String categrayFileId, LoginUser currentUser);

	
	public void physicsDelFile(String categrayFileId, LoginUser currentUser);
}