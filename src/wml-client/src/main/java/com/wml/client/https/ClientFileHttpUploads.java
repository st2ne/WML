package com.wml.client.https;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.BufferedSink;

public class ClientFileHttpUploads {

	private String uploadUrl;
	private String progressUrl;
	private ProgressHandle handles;
	private Map<String, String> baseParas;
	private Map<String, String> uploadParas;
	private Map<String, String> getProgressParas;
	private String processkey;

	private long filesize;
	private String filename;
	private InputStream inStream = null;

	public static ClientFileHttpUploads getInstance(String url, Map<String, String> param) {
		ClientFileHttpUploads obj = new ClientFileHttpUploads();
		obj.uploadUrl = url;
		obj.baseParas = param;
		obj.processkey = param.get("processkey");
		HashMap<String, String> newmap = new HashMap<>();
		newmap.putAll(param);
		obj.uploadParas = newmap;
		return obj;
	}

	/**
	 * 设置获取远程处理进度的url
	 * 
	 * @param url
	 */
	public void setRemoteProgressUrl(String url) {
		progressUrl = url;
		if (processkey == null) {
			processkey = UUID.randomUUID().toString().replaceAll("-", "");
		}
		HashMap<String, String> newmap = new HashMap<>();
		newmap.putAll(baseParas);
		getProgressParas = newmap;
	}

	public ClientFileHttpUploads setProgressHandle(ProgressHandle handles) {
		this.handles = handles;
		return this;
	}

	/**
	 * 執行上傳
	 * 
	 * @param upFile   附件
	 * @param filename 文件名称
	 * @param syncid   判断文件是否重复的同步ID
	 * @return
	 * @throws IOException
	 */
	public JSONObject doUpload(File upFile, String filename) throws IOException {
		if (!upFile.exists()) {
			throw new RuntimeException("the file is not exist:" + upFile.getPath());
		}
		return doUpload(upFile.getName(), upFile.length(), new FileInputStream(upFile));
	}

	/**
	 * 執行上傳
	 * 
	 * @param upFile   附件
	 * @param filename 文件名称
	 * @param syncid   判断文件是否重复的同步ID
	 * @return
	 * @throws IOException
	 */
	public JSONObject doUpload(String filename, long size, InputStream inStream) throws IOException {
		try {
			this.filesize = size;
			this.filename = filename;
			this.inStream = inStream;
			// 启动远程执行函数
			startGetProgressTheard();
			uploadParas.put("processkey", processkey);
			uploadParas.put("filename", filename);
			String result = post(uploadUrl, uploadParas, inStream, handles);
			JSONObject obj = new JSONObject(result);
			if (obj.getInt("STATE") == 0) {
				return obj;
			} else {
				throw new RuntimeException(obj.getString("MESSAGE"));
			}
		} finally {
			// 关闭远程执行函数
			start = 2;
			if (inStream != null) {
				inStream.close();
			}
		}
	}

	// 0未开始上传，1文件传输完成，2远程处理完成（全部完成）
	private int start = 0;

	private void startGetProgressTheard() {
		new Thread() {
			public void run() {
				while (start < 2) {
					try {// 文件未全部上传完毕
						if (start > 0) {
							// 文件以及上传，等待远程处理
							HashMap<String, String> newMap = new HashMap<>();
							newMap.putAll(getProgressParas);
							newMap.put("processkey", processkey);
							JSONObject json = HttpUtils.httpPost(progressUrl, newMap);
							if (handles != null) {
								String state = json.get("STATE").toString();
								if (state.equals("0")) {
									handles.handle(50 + json.getInt("PROCESS") / 2,
											filesize * json.getInt("PROCESS") / 100, filesize, "REMOTE");
								}
							}
						}
						Thread.sleep(500);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}

	public String post(String url, Map<String, String> map, final InputStream inStream, final ProgressHandle hanle)
			throws IOException {
		OkHttpClient client = new OkHttpClient.Builder().connectTimeout(20 * 1000, TimeUnit.MILLISECONDS)
				.readTimeout(20 * 60 * 1000, TimeUnit.MILLISECONDS).writeTimeout(20 * 60 * 1000, TimeUnit.MILLISECONDS)
				.build();
		MultipartBody.Builder builder = new MultipartBody.Builder();
		if (map != null) {
			for (Map.Entry<String, String> entry : map.entrySet()) {
				builder.addFormDataPart(entry.getKey(), entry.getValue().toString());
			}
		}
		RequestBody fileBody = new RequestBody() {
			@Override
			public long contentLength() throws IOException {
				return filesize;
			}

			@Override
			public MediaType contentType() {
				String TYPE = "application/octet-stream";
				return MediaType.parse(TYPE);
			}

			@Override
			public void writeTo(BufferedSink sink) throws IOException {
				int byteread = 0;
				if (inStream != null) { // 文件存在时
					try {
						byte[] buffer = new byte[9999];
						int readed = 0;
						int lastpressent = 0;
						while ((byteread = inStream.read(buffer)) != -1) {
							readed = readed + buffer.length;
							int percentage = (int) (1.0 * readed / filesize * 100);
							if (percentage > 99) {
								start = 1;
							}
							if (hanle != null) {
								int sendInt = percentage;
								if (StringUtils.isNotBlank(progressUrl)) {
									sendInt = sendInt / 2;
								}
								if (lastpressent != sendInt) {
									hanle.handle(sendInt, readed, filesize, "UPLOAD");
								}
								lastpressent = sendInt;
							}
							sink.write(buffer, 0, byteread);
						}
					} finally {
						// sink.close();不要关闭，关闭会导致上传错误
						inStream.close();
					}
				}
			}
		};
		RequestBody requestBody = builder.setType(MultipartBody.FORM).addFormDataPart("file", filename, fileBody)
				.build();
		Request request = new Request.Builder().url(url).post(requestBody).build();
		Response response = client.newCall(request).execute();
		return response.body().string();
	}

	/**
	 * 上传进度的回调方法
	 * 
	 * @author macpl
	 *
	 */
	public interface ProgressHandle {
		/**
		 * @param percent
		 * @param allsize
		 * @param completesize
		 * @param state        0开始上传，1远程处理中，2全部完成
		 */
		public void handle(int percent, long allsize, long completesize, String state);
	}

}
