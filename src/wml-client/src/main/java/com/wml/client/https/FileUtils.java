package com.wml.client.https;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class FileUtils {
	private static final Logger log = Logger.getLogger(FileUtils.class);
	private static final Pattern FilePattern = Pattern.compile("[\\\\/:*?\"<>|\\\\.]");

	/**
	 * 过滤文件名中的特殊字符
	 * 
	 * @param str
	 * @return
	 */
	public static String filenameFilter(String str) {
		return str == null ? null : FilePattern.matcher(str).replaceAll("");
	}

	/**
	 * 拷贝一个文件到新的地址
	 * 
	 * @param file
	 * @param newPath
	 */
	public static void copyFile(File file, String newPath, String newFileName) {
		int byteread = 0;
		File oldfile = file;
		if (oldfile.exists()) { // 文件存在时
			InputStream inStream = null;
			FileOutputStream fs = null;
			try {
				inStream = new FileInputStream(file);
				if (newFileName == null) {
					newFileName = file.getName();
				}
				fs = new FileOutputStream(newPath + newFileName);
				byte[] buffer = new byte[1444];
				while ((byteread = inStream.read(buffer)) != -1) {
					fs.write(buffer, 0, byteread);
				}
			} catch (FileNotFoundException e) {
				log.error(e.getMessage());
			} catch (IOException e) {
				log.error(e.getMessage());
			} finally {
				try {
					inStream.close();
					fs.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public static void copyFile(File file, File newfile, String fileUpdataProcessKey) {
		int byteread = 0;
		File oldfile = file;
		if (oldfile.exists()) { // 文件存在时
			InputStream inStream = null;
			FileOutputStream fs = null;
			try {
				inStream = new FileInputStream(file);
				fs = new FileOutputStream(newfile);
				byte[] buffer = new byte[1444];
				int readed = 0;
				while ((byteread = inStream.read(buffer)) != -1) {
					if (StringUtils.isNotBlank(fileUpdataProcessKey)) {
						readed = readed + buffer.length;
						int percentage = (int) (1.0 * readed / file.length() * 100);
						if (percentage > 100) {
							percentage = 100;
						}
						FileCopyProcessCache.setProcess(fileUpdataProcessKey, percentage);
					}
					fs.write(buffer, 0, byteread);
				}
			} catch (FileNotFoundException e) {
				log.error(e.getMessage());
			} catch (IOException e) {
				log.error(e.getMessage());
			} finally {
				try {
					inStream.close();
					fs.close();
				} catch (IOException e) {
				}
			}
		}
	}

	/**
	 * 将文件流保存到一个地址
	 * 
	 * @param inStream
	 * @param filename
	 * @param newPath
	 * @return
	 */
	public static Long saveFile(InputStream inStream, File newfile) {
		int byteread = 0;
		FileOutputStream fs = null;
		try {
			fs = new FileOutputStream(newfile);
			byte[] buffer = new byte[1444];
			while ((byteread = inStream.read(buffer)) != -1) {
				fs.write(buffer, 0, byteread);
			}
		} catch (FileNotFoundException e) {
			log.error(e.getMessage());
		} catch (IOException e) {
			log.error(e.getMessage());
		} finally {
			try {
				inStream.close();
				fs.close();
			} catch (IOException e) {
			}
		}
		return newfile.length();
	}

	public static long saveFile(byte[] fileData, File newfile) {
		FileOutputStream fs = null;
		try {
			fs = new FileOutputStream(newfile);
			fs.write(fileData);
			fs.flush();
		} catch (FileNotFoundException e) {
			log.error(e.getMessage());
		} catch (IOException e) {
			log.error(e.getMessage());
		} finally {
			try {
				fs.close();
			} catch (IOException e) {
			}
		}
		return newfile.length();
	}

	//
	/**
	 * 文件转base64字符串
	 * 
	 * @param file
	 * @return
	 */
	public static String getBase64(File file) {
		InputStream inputStream = null;
		byte[] data = null;
		try {
			inputStream = new FileInputStream(file);
			data = new byte[inputStream.available()];
			inputStream.read(data);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// 加密
		String baseStr = Base64.encodeBase64String(data).trim();
		return baseStr;
	}

	/**
	 * 文件转byte[]
	 * 
	 * @param file
	 * @return
	 */
	public static byte[] getByteArray(File file) {
		InputStream inputStream = null;
		byte[] data = null;
		try {
			inputStream = new FileInputStream(file);
			data = new byte[inputStream.available()];
			inputStream.read(data);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// 加密
		return data;
	}
}
