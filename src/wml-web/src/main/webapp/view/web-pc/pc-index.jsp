<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><PF:ParameterValue key="config.sys.title" /></title>
<meta name="description"
	content='<PF:ParameterValue key="config.sys.mate.description"/>'>
<meta name="keywords"
	content='<PF:ParameterValue key="config.sys.mate.keywords"/>'>
<meta name="author"
	content='<PF:ParameterValue key="config.sys.mate.author"/>'>
<meta name="robots" content="index,follow">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<jsp:include page="atext/include-pc-web.jsp"></jsp:include>
<link href="text/lib/jstree/themes/default/style.css" rel="stylesheet">
<script src="text/lib/jstree/jstree.min.js"></script>
<jsp:include page="commons/pc-style.jsp"></jsp:include>
</head><!-- oncontextmenu="self.event.returnValue=false" -->
<body style="display: none;" class="wml-pc-body" >
	<DIV id="wml-ui-center-id" class="ui-layout-center wml-ui-l1-center">
		<DIV class="ui-layout-north wml-ui-l2-center-top">
			<jsp:include page="commons/pc-center-top.jsp"></jsp:include>
		</DIV>
		<DIV id="wml-ui-center-l2-id" class="ui-layout-center">
			<DIV class="ui-layout-north"
				style="background-color: #353639; border-top: 1px dashed #000000;">
				<jsp:include page="commons/pc-center-top-pluswin.jsp"></jsp:include>
			</DIV>
			<DIV class="ui-layout-center">
				<jsp:include page="commons/pc-center.jsp"></jsp:include>
			</DIV>
		</DIV>
	</DIV>
	<DIV class="ui-layout-east wml-ui-l1-east">
		<div style="height: 100%;width: 100%;overflow: auto;">
			<jsp:include page="commons/pc-right.jsp"></jsp:include></div>
	</DIV>
	<DIV id="wml-ui-left-id" class="ui-layout-west wml-ui-l1-west">
		<DIV class="ui-layout-north">
			<jsp:include page="commons/pc-left-top.jsp"></jsp:include>
		</DIV>
		<DIV class="ui-layout-center">
			<jsp:include page="commons/pc-left-center.jsp"></jsp:include>
		</DIV>
	</DIV>
	<jsp:include page="/view/web-pc/func/includeWindows.jsp"></jsp:include>
</body>
<script type="text/javascript">
	var wml_ui_innerCenter;
	$(function() {
		$('body').show();
		$('body').layout({
			west__closable : false,//可以被关闭  
			west__resizable : false,//可以改变大小
			west__size : 270,
			west__spacing_open : 1,

			east__closable : true,//可以被关闭  
			east__resizable : false,//可以改变大小
			east__size : 280,
			east__spacing_open : 1
		});

		$('#wml-ui-left-id').layout({
			north__closable : false,//可以被关闭  
			north__resizable : false,//可以改变大小
			north__size : 50,
			north__spacing_open : 0
		});

		$('#wml-ui-center-id').layout({
			north__closable : false,//可以被关闭  
			north__resizable : false,//可以改变大小
			north__size : 50,
			north__spacing_open : 0
		});

		wml_ui_innerCenter = $('#wml-ui-center-l2-id').layout({
			north__closable : true,//可以被关闭  
			north__resizable : false,//可以改变大小
			north__size : 100,
			north__spacing_open : 1
		});
		closeTopMenuWin();
		clickWmlCategray("NONE");
	});
</script>
<jsp:include page="commons/pc-js-funcs.jsp"></jsp:include>
</html>