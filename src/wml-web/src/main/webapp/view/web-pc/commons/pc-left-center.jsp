<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div style="overflow: auto; height: 100%; user-select: none;">
	<div style="padding: 10px; margin-top: 10px;" class="wml-ui-dir-search">
		<div class="input-group input-group-sm mb-3">
			<div class="input-group-prepend">
				<span class="input-group-text" id="inputGroup-sizing-sm"><i
					class="bi bi-search"></i></span>
			</div>
			<input type="text" class="form-control" id="categraySearcheInputId"
				placeholder="查找素材分类" aria-label="Sizing example input"
				aria-describedby="inputGroup-sizing-sm">
		</div>
	</div>
	<div class="wml-ui-left-menus">
		<div class="menus-node">
			<a onclick="clickWmlCategray('NONE');"> <i
				class="bi bi-file-ruled"></i>&nbsp;资源素材库
			</a>
		</div>
		<div id="data" class="demo"></div>
		<hr />
		<div class="menus-node">
			<a onclick="clickWmlCategray('FAVORITES');freshFavoritesNum();">
				<i class="bi bi-bag-heart"></i>&nbsp;文件收藏 <span id="favoritesNumId"></span>

			</a>
		</div>
		<div class="menus-node">
			<a onclick="clickWmlCategray('BOOK');freshBookNum();"> <i
				class="bi bi-bookmark-check"></i>&nbsp;分类订阅 <span id="bookNumId"></span>
			</a>
		</div>
		<div class="menus-node">
			<a onclick="clickWmlCategray('VISIT');freshVisitNum();"> <i
				class="bi bi-clock"></i>&nbsp;最近访问 <span id="visitNumId"></span>
			</a>
		</div>
		<div class="menus-node">
			<a onclick="clickWmlCategray('RECYCLE');"> <i
				class="bi bi-bag-x"></i>&nbsp;回收站 <span id="recycleNumId"></span>
			</a>
		</div>
		<hr />
		<div class="menus-node">
			<a href="userspace/settinginfo.do"> <i class="bi bi-person-gear"></i>&nbsp;个人设置
			</a>
		</div>
		<c:if test="${USEROBJ.type=='3'}">
			<div class="menus-node">
				<a href="frame/index.do"> <i class="bi bi-gear"></i>&nbsp;管理控制台
				</a>
			</div>
		</c:if>
	</div>
	<script>
		function selectTreeNode(nodeid) {
			$('#data').jstree('deselect_all', false);
			$('#data').jstree('open_node', nodeid);

		}
		$(function() {
			$('#data').on("select_node.jstree", function(e, data) {
				clickWmlCategray(data.selected)
			}).jstree({
				'core' : {
					'data' : {
						"url" : "ui/categrays.do",
						"dataType" : "json" // needed only if you do not supply JSON headers
					}
				}
			});
			freshFavoritesNum();
			freshVisitNum();
			freshBookNum();
		});
		$('#categraySearcheInputId').bind('keypress', function(event) {
			if (event.keyCode == "13") {
				loadTrees(($(this).val().trim()));
			}
		});
		function freshFavoritesNum() {
			$.post('opfile/vlognum.do', {
				'opname' : 'FAVORITES'
			}, function(jsonObject) {
				if (jsonObject.STATE == 0) {
					$('#favoritesNumId').html('');
					if (jsonObject.num > 0) {
						$('#favoritesNumId').html(
								'<span class="badge badge-success">'
										+ jsonObject.num + '</span>');
					}
				} else {
					alert(jsonObject.MESSAGE);
				}
			}, 'json');
		}

		function freshVisitNum() {
			$.post('opfile/vlognum.do', {
				'opname' : 'VISIT'
			}, function(jsonObject) {
				if (jsonObject.STATE == 0) {
					$('#visitNumId').html('');
					if (jsonObject.num > 0) {
						$('#visitNumId').html(
								'<span class="badge badge-warning">'
										+ jsonObject.num + '</span>');
					}
				} else {
					alert(jsonObject.MESSAGE);
				}
			}, 'json');
		}
		
		function freshBookNum() {
			$.post('opfile/vlognum.do', {
				'opname' : 'BOOK'
			}, function(jsonObject) {
				if (jsonObject.STATE == 0) {
					$('#bookNumId').html('');
					if (jsonObject.num > 0) {
						$('#bookNumId').html(
								'<span class="badge badge-info">'
										+ jsonObject.num + '</span>');
					}
				} else {
					alert(jsonObject.MESSAGE);
				}
			}, 'json');
		}
		
		
	</script>
</div>