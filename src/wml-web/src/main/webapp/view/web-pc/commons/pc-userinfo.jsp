<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="media" style="margin: 14px; margin-bottom: 4px;">
	<img src="download/PubPhoto.do?id=${USEROBJ.imgid}" class="mr-3"
		style="width: 24px; height: 24px; position: relative; top: -2px; border-radius: 25px;"
		alt="...">
	<div class="media-body">
		<h4 class="media-heading" style="font-size: 14px;">
			<a href="userspace/settinginfo.do">${USEROBJ.name}</a>
			&nbsp;<a href="login/webout.do" style="float: right;"><i
				class="glyphicon glyphicon-log-out"></i>&nbsp;注销</a>
		</h4>
	</div>
</div>
<hr style="margin-top: 10px;" />