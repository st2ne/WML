<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<input type="hidden" id="currentCategrayId" value="${categray.id}">
<style>
.wml_ui_list_file {
	font-size: 12px;
	margin-bottom: 20px;
}

.wml_ui_list_file .imgbox {
	height: 150px;
}

.wml_ui_list_file h5 {
	font-size: 16px;
}

.wml_ui_list_file .tag {
	
}

.wml-ui-filsbox .light {
	margin-bottom: 5px;
	padding-top: 5px;
	cursor: pointer;
	user-select: none;
}

.wml-ui-filsbox .light:hover {
	background-color: #424447;
	border-radius: 5px;
}

.wml-ui-filsbox .light:active {
	background-color: #525457;
	border-radius: 5px;
}

.wml-ui-filsbox .light.active {
	background-color: #424447;
	border-radius: 5px;
}

.wml-ui-filsbox .light.mactive {
	background-color: #666666;
	border-radius: 5px;
}
</style>
<div class="container-fluid wml-ui-filsbox"
	style="margin-bottom: 50px; padding: 30px; user-select: none;">
	<div class="row">
		<c:if test="${!empty parentId&&parentId!='NONE' }">
			<div class="col-${col_num} light wml_ui_list_file ${parentId}"
				onclick="loadWmlCategrayInfo('${parentId}')"
				ondblclick="clickWmlCategray('${parentId}')">
				<div class="media">
					<img src="text/img/fileicon/DIR.png"
						style="height: 48px; max-width: 128px;" class="mr-3" alt="...">
					<div class="media-body ">
						<h5 class="mt-0" title="返回上级分类">..</h5>
						<p>返回上级分类</p>
					</div>
				</div>
			</div>
		</c:if>
		<c:forEach items="${dirs}" var="node">
			<div class="col-${col_num} light wml_ui_list_file ${node.appid}"
				onclick="loadWmlCategrayInfo('${node.appid}')"
				ondblclick="clickWmlCategray('${node.appid}')">
				<div class="media">
					<img src="text/img/fileicon/DIR.png"
						style="height: 48px; max-width: 128px;" class="mr-3" alt="...">
					<div class="media-body ">
						<h5 class="mt-0" title="${node.title}">${node.title}</h5>
						<p title="文件数量">${node.filenum }/${node.allfilenum }&nbsp;&nbsp;${node.ctime }</p>
					</div>
				</div>
			</div>
		</c:forEach>
		<c:forEach items="${files}" var="node">
			<div class="col-${col_num} light wml_ui_list_file ${node.appid}"
				data-fileid="${node.appid}"
				onclick="loadWmlFileInfo('${node.appid}')"
				ondblclick="dblclickFile('${node.title}','${node.id}','${node.modellabel}','${node.exname}')">
				<div class="media">
					<img src="${node.iconUrl}" style="height: 48px; max-width: 128px;"
						class="mr-3" alt="...">
					<div class="media-body ">
						<h5 class="mt-0" title="${node.title}">${node.title}</h5>
						<p>${node.lenTag }&nbsp;&nbsp;${node.ctime }</p>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
	<c:if test="${empty files&&empty  dirs}">
		<jsp:include page="/view/web-pc/infos/pc-categray-files-none.jsp"></jsp:include>
	</c:if>
</div>
<script type="text/javascript">
	$(function() {
		initTopButtons('${categrayKey}');
	});
</script>