<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--预加载参数表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formParareadypara">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<c:if test="${pageset.operateType==2}">
					<tr>
						<td class="title">名称:</td>
						<td colspan="3">${paraDefine.name}</td>
					</tr>
					<tr>
						<td class="title">KEY:</td>
						<td colspan="3">${paraDefine.pkey}</td>
					</tr>
					<tr>
						<td class="title">值:</td>
						<td colspan="3"><input type="text" style="width: 360px;"
							class="easyui-validatebox"
							data-options="required:true,validType:[,'maxLength[256]']"
							id="entity_pval" name="pval" value="${entity.pval}"></td>
					</tr>
					<tr>
						<td class="title">注释:</td>
						<td colspan="3">${paraDefine.note}</td>
					</tr>
				</c:if>
				<c:if test="${pageset.operateType==0}">
					<tr>
						<td class="title">备注:</td>
						<td colspan="3"><textarea style="width: 360px;"
								class="easyui-validatebox"
								data-options="validType:[,'maxLength[512]']"
								id="entity_comments" name="comments">${entity.comments}</textarea></td>
					</tr>
				</c:if>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityParareadypara" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityParareadypara" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formParareadypara" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionParareadypara = 'parareadypara/add.do';
	var submitEditActionParareadypara = 'parareadypara/edit.do';
	var currentPageTypeParareadypara = '${pageset.operateType}';
	var submitFormParareadypara;
	$(function() {
		//表单组件对象
		submitFormParareadypara = $('#dom_formParareadypara').SubmitForm({
			pageType : currentPageTypeParareadypara,
			grid : gridParareadypara,
			currentWindowId : 'winParareadypara'
		});
		//关闭窗口
		$('#dom_cancle_formParareadypara').bind('click', function() {
			$('#winParareadypara').window('close');
		});
		//提交新增数据
		$('#dom_add_entityParareadypara').bind('click', function() {
			submitFormParareadypara.postSubmit(submitAddActionParareadypara);
		});
		//提交修改数据
		$('#dom_edit_entityParareadypara').bind('click', function() {
			submitFormParareadypara.postSubmit(submitEditActionParareadypara);
		});
	});
//-->
</script>