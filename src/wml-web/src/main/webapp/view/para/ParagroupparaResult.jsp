<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north',border:false">
		<form id="searchParagroupparaForm">
			<table class="editTable">
				<tr style="text-align: center;">
					<td class="title">名称:</td>
					<td><input name="NAME:like" type="text"></td>
					<td class="title">KEY:</td>
					<td><input name="PKEY:like" type="text"></td>
					<td><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataParagroupparaGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="NAME" data-options="sortable:true" width="40">参数名称</th>
					<th field="PKEY" data-options="sortable:true" width="40">PKEY</th>
					<th field="PVAL" data-options="sortable:true" width="40">参数值</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var url_delActionParagrouppara = "paragrouppara/del.do";//删除URL
	var url_formActionParagrouppara = "paragrouppara/form.do";//增加、修改、查看URL
	var url_searchActionParagrouppara = "paragrouppara/query.do?groupid=${groupid}";//查询URL
	var title_windowParagrouppara = "参数组参数管理";//功能名称
	var gridParagrouppara;//数据表格对象
	var searchParagrouppara;//条件查询组件对象
	var toolBarParagrouppara = [// {
	//	id : 'view',
	//	text : '查看',
	//	iconCls : 'icon-tip',
	//		handler : viewDataParagrouppara
	//	},
	{
		id : 'add',
		text : '添加参数',
		iconCls : 'icon-add',
		handler : addDataParagrouppara
	}, {
		id : 'edit',
		text : '修改参数值',
		iconCls : 'icon-edit',
		handler : editDataParagrouppara
	}, {
		id : 'del',
		text : '移除参数',
		iconCls : 'icon-remove',
		handler : delDataParagrouppara
	} ];
	$(function() {
		//初始化数据表格
		gridParagrouppara = $('#dataParagroupparaGrid').datagrid({
			url : url_searchActionParagrouppara,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarParagrouppara,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchParagrouppara = $('#searchParagroupparaForm').searchForm({
			gridObj : gridParagrouppara
		});
	});
	//查看
	function viewDataParagrouppara() {
		var selectedArray = $(gridParagrouppara).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionParagrouppara + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winParagrouppara',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataParagrouppara() {
		chooseWindowCallBackHandle = function(rows) {
			var ids = "";
			$(rows).each(function(i, obj) {
				ids = ids + "," + obj.ID;
			});
			$.post('paragrouppara/addParaDefine.do?groupid=${groupid}&ids='
					+ ids, {}, function(flag) {
				var jsonObject = JSON.parse(flag, null);
				$(gridParagrouppara).datagrid('loaded');
				if (jsonObject.STATE == 0) {
					$(gridParagrouppara).datagrid('reload');
					$("#chooseParadefineWin").window('close');
				} else {
					var str = MESSAGE_PLAT.ERROR_SUBMIT + jsonObject.MESSAGE;
					$.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
				}
			});
		};
		$.farm.openWindow({
			id : 'chooseParadefineWin',
			width : 700,
			height : 400,
			modal : true,
			url : 'paradefine/ParadefineChooseGridPage.do',
			title : '参数定义'
		});
	}
	//修改
	function editDataParagrouppara() {
		var selectedArray = $(gridParagrouppara).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionParagrouppara + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winParagrouppara',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataParagrouppara() {
		var selectedArray = $(gridParagrouppara).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridParagrouppara).datagrid('loading');
					$.post(url_delActionParagrouppara + '?ids='
							+ $.farm.getCheckedIds(gridParagrouppara, 'ID'),
							{}, function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridParagrouppara).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridParagrouppara).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>