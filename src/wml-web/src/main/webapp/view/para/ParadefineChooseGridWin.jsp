<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--参数定义-->
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north',border:false">
		<form id="dom_chooseSearchParadefine">
			<table class="editTable">
				<tr>
					<td class="title">名称:</td>
					<td><input name="NAME:like" type="text"></td>
					<td class="title">KEY:</td>
					<td><input name="PKEY:like" type="text"></td>
					<td><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table class="easyui-datagrid" id="dom_chooseGridParadefine">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="NAME" data-options="sortable:true" width="40">名称</th>
					<th field="PKEY" data-options="sortable:true" width="40">PKEY</th>
					<th field="NOTE" data-options="sortable:true" width="40">注释</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var chooseGridParadefine;
	var chooseSearchfarmParadefine;
	var toolbar_chooseParadefine = [ {
		text : '选择',
		iconCls : 'icon-ok',
		handler : function() {
			var selectedArray = $('#dom_chooseGridParadefine').datagrid(
					'getSelections');
			if (selectedArray.length > 0) {
				chooseWindowCallBackHandle(selectedArray);
			} else {
				$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
						'info');
			}
		}
	} ];
	$(function() {
		chooseGridParadefine = $('#dom_chooseGridParadefine').datagrid({
			url : 'paradefine/ParadefineChooseQuery.do',
			fit : true,
			'toolbar' : toolbar_chooseParadefine,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			striped : true,
			rownumbers : true,
			ctrlSelect : true,
			fitColumns : true
		});
		chooseSearchfarmParadefine = $('#dom_chooseSearchParadefine')
				.searchForm({
					gridObj : chooseGridParadefine
				});
	});
//-->
</script>







<!--1.在调用JSP页面，中粘贴下面js中的一段（绑定到按钮事件，或通过方法打开窗口） 
//---------------------------使用下面的（绑定到按钮事件）----------------------------------------------------- 
<a id="buttonParadefineChoose" href="javascript:void(0)" class="easyui-linkbutton" style="color: #000000;">选择</a>
<script type="text/javascript">
  $(function() {
    $('#buttonParadefineChoose').bindChooseWindow('chooseParadefineWin', {
      width : 600,
      height : 300,
      modal : true,
      url : 'paradefine/ParadefineChooseGridPage.do',
      title : '参数定义',
      callback : function(rows) {
        //$('#NAME_like').val(rows[0].NAME);
      }
    });
  });
</script>
//----------------------或----使用下面的（窗口中打开）----------------------------------------------------- 
chooseWindowCallBackHandle = function(row) {
    $("#chooseParadefineWin").window('close');  
};
$.farm.openWindow( {
  id : 'chooseParadefineWin',
  width : 600,
  height : 300,
  modal : true,
  url : 'paradefine/ParadefineChooseGridPage.do',
  title : '参数定义'
});
 -->





