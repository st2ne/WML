<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--附件功能日志表单-->
<div class="easyui-layout" data-options="fit:true">
  <div class="TableTitle" data-options="region:'north',border:false">
    <span class="label label-primary"> 
    <c:if test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if>
    <c:if test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if>
    <c:if test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
    </span>
  </div>
  <div data-options="region:'center'">
    <form id="dom_formVisitlogs">
      <input type="hidden" id="entity_id" name="id" value="${entity.id}">
      <table class="editTable">
      <tr>
        <td class="title">
        STRFLAG:
        </td>
        <td colspan="3">
          <input type="text" style="width: 360px;" class="easyui-validatebox" data-options="validType:[,'maxLength[128]']"
          id="entity_strflag" name="strflag" value="${entity.strflag}">
        </td>
      </tr>
      <tr>
        <td class="title">
        INTFLAG:
        </td>
        <td colspan="3">
          <input type="text" style="width: 360px;" class="easyui-validatebox" data-options="validType:[,'maxLength[5]']"
          id="entity_intflag" name="intflag" value="${entity.intflag}">
        </td>
      </tr>
      <tr>
        <td class="title">
        TYPE:
        </td>
        <td colspan="3">
          <select name="type" id="entity_type"  val="${entity.type}"><option value="0">NONE</option></select>
        </td>
      </tr>
      <tr>
        <td class="title">
        CUSER:
        </td>
        <td colspan="3">
          <input type="text" style="width: 360px;" class="easyui-validatebox" data-options="required:true,validType:[,'maxLength[16]']"
          id="entity_cuser" name="cuser" value="${entity.cuser}">
        </td>
      </tr>
      <tr>
        <td class="title">
        CTIME:
        </td>
        <td colspan="3">
          <input type="text" style="width: 360px;" class="easyui-validatebox" data-options="required:true,validType:[,'maxLength[7]']"
          id="entity_ctime" name="ctime" value="${entity.ctime}">
        </td>
      </tr>
    </table>
    </form>
  </div>
  <div data-options="region:'south',border:false">
    <div class="div_button" style="text-align: center; padding: 4px;">
      <c:if test="${pageset.operateType==1}">
      <a id="dom_add_entityVisitlogs" href="javascript:void(0)"  iconCls="icon-save" class="easyui-linkbutton">增加</a>
      </c:if>
      <c:if test="${pageset.operateType==2}">
      <a id="dom_edit_entityVisitlogs" href="javascript:void(0)" iconCls="icon-save" class="easyui-linkbutton">修改</a>
      </c:if>
      <a id="dom_cancle_formVisitlogs" href="javascript:void(0)" iconCls="icon-cancel" class="easyui-linkbutton"   style="color: #000000;">取消</a>
    </div>
  </div>
</div>
<script type="text/javascript">
  var submitAddActionVisitlogs = 'visitlogs/add.do';
  var submitEditActionVisitlogs = 'visitlogs/edit.do';
  var currentPageTypeVisitlogs = '${pageset.operateType}';
  var submitFormVisitlogs;
  $(function() {
    //表单组件对象
    submitFormVisitlogs = $('#dom_formVisitlogs').SubmitForm( {
      pageType : currentPageTypeVisitlogs,
      grid : gridVisitlogs,
      currentWindowId : 'winVisitlogs'
    });
    //关闭窗口
    $('#dom_cancle_formVisitlogs').bind('click', function() {
      $('#winVisitlogs').window('close');
    });
    //提交新增数据
    $('#dom_add_entityVisitlogs').bind('click', function() {
      submitFormVisitlogs.postSubmit(submitAddActionVisitlogs);
    });
    //提交修改数据
    $('#dom_edit_entityVisitlogs').bind('click', function() {
      submitFormVisitlogs.postSubmit(submitEditActionVisitlogs);
    });
  });
  //-->
</script>