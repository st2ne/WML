<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>资源库数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false">
		<form id="searchFileresourceForm">
			<table class="editTable">
				<tr style="text-align: center;">
					<td colspan="4"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataFileresourceGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="TITLE" data-options="sortable:true" width="20">资源库名称</th>
					<th field="PATH" data-options="sortable:true" width="50">目录</th>
					<th field="SPACEINFO" data-options="sortable:true" width="50">空间</th>
					<th field="STATE" data-options="sortable:true" width="20">状态</th>
					<th field="ETIME" data-options="sortable:true" width="20">编辑时间</th>
				</tr>
			</thead>
		</table>
	</div>
</body>
<script type="text/javascript">
	var url_delActionFileresource = "fileresource/del.do";//删除URL
	var url_formActionFileresource = "fileresource/form.do";//增加、修改、查看URL
	var url_searchActionFileresource = "fileresource/query.do";//查询URL
	var title_windowFileresource = "资源库管理";//功能名称
	var gridFileresource;//数据表格对象
	var searchFileresource;//条件查询组件对象
	var toolBarFileresource = [ /**{
		id : 'view',
		text : '查看',
		iconCls : 'icon-tip',
		handler : viewDataFileresource
	}, **/{
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataFileresource
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataFileresource
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataFileresource
	} ];
	$(function() {
		//初始化数据表格
		gridFileresource = $('#dataFileresourceGrid').datagrid({
			url : url_searchActionFileresource,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarFileresource,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchFileresource = $('#searchFileresourceForm').searchForm({
			gridObj : gridFileresource
		});
	});
	//查看
	function viewDataFileresource() {
		var selectedArray = $(gridFileresource).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionFileresource + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winFileresource',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataFileresource() {
		var url = url_formActionFileresource + '?operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winFileresource',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataFileresource() {
		var selectedArray = $(gridFileresource).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionFileresource + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winFileresource',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataFileresource() {
		var selectedArray = $(gridFileresource).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridFileresource).datagrid('loading');
					$.post(url_delActionFileresource + '?ids='
							+ $.farm.getCheckedIds(gridFileresource, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridFileresource).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridFileresource).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>