<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%><%@ taglib
	uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<div
	style="background-color: #ffffff; padding: 0px; border-radius: 1em; color: #666666; border-radius: 5px; overflow: hidden;">
	<div class="row">
		<div class="col-md-4">
			<div class="media" style="margin: 25px; margin-left: 30px;">
				<div class="media-left">
					<img alt="" src="download/PubPhoto.do?id=${USEROBJ.imgid}"
						style="width: 64px; height: 64px; position: relative; top: -2px;"
						class="img-circle">
				</div>
				<div class="media-body">
					<h4 class="media-heading">${USEROBJ.name}</h4>
					<div style="font-size: 12px; color: #999999; margin: 0px;">
						<span style="color: #666666;">组织机构</span>：${org.name }<br /> <span
							style="color: #666666;">角色</span>:
						<c:forEach items="${posts}" var="node">
												${node.name}&nbsp;&nbsp;
											</c:forEach>
					<div
						style="word-break: break-all; font-size: 10px; color: #aaaaaa;">ID:${user.id}</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<table class="table table-striped wuc-userinfo"
				style="margin-top: 20px;">
				<tbody>
					<tr>
						<th>登陆名</th>
						<th>状态</th>
						<th>用户类型</th>
						<th>登陆时间</th>
					</tr>
					<tr>
						<td style="word-break: break-all;">${user.loginname}</td>
						<td><c:if test="${user.state=='1'}">可用</c:if> <c:if
								test="${user.state=='0'}">禁用</c:if> <c:if
								test="${user.state=='2'}">删除</c:if> <c:if
								test="${user.state=='3'}">待审核</c:if></td>
						<td><c:if test="${user.type=='1'}">系统用户</c:if> <c:if
								test="${user.type=='2'}">接口用户</c:if> <c:if
								test="${user.type=='3'}">超级用户</c:if></td>
						<td><PF:FormatTime date="${user.logintime}"
								yyyyMMddHHmmss="yyyy-MM-dd" /></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>