<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>分类文件数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false">
		<form id="searchCategrayfileForm">
			<table class="editTable">
				<tr>
					<td class="title">ID:</td>
					<td><input name="ID:like" type="text"></td>
					<td class="title"></td>
					<td></td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="4"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataCategrayfileGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="FILENAME" data-options="sortable:true" width="100">文件名称</th>
					<th field="CATEGRAYNAME" data-options="sortable:true" width="100">分类名称</th>
					<th field="FILEID" data-options="sortable:true" width="50">FILEID</th>
					<th field="CATEGRAYID" data-options="sortable:true" width="50">CATEGRAYID</th>
					<th field="ID" data-options="sortable:true" width="50">CATEGRAYFILEID</th>
				</tr>
			</thead>
		</table>
	</div>
</body>
<script type="text/javascript">
	var url_delActionCategrayfile = "categrayfile/del.do";//删除URL
	var url_formActionCategrayfile = "categrayfile/form.do";//增加、修改、查看URL
	var url_searchActionCategrayfile = "categrayfile/query.do";//查询URL
	var title_windowCategrayfile = "分类文件管理";//功能名称
	var gridCategrayfile;//数据表格对象
	var searchCategrayfile;//条件查询组件对象
	var toolBarCategrayfile = [ {
		id : 'view',
		text : '查看',
		iconCls : 'icon-tip',
		handler : viewDataCategrayfile
	}, {
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataCategrayfile
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataCategrayfile
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataCategrayfile
	} ];
	$(function() {
		//初始化数据表格
		gridCategrayfile = $('#dataCategrayfileGrid').datagrid({
			url : url_searchActionCategrayfile,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarCategrayfile,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchCategrayfile = $('#searchCategrayfileForm').searchForm({
			gridObj : gridCategrayfile
		});
	});
	//查看
	function viewDataCategrayfile() {
		var selectedArray = $(gridCategrayfile).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionCategrayfile + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winCategrayfile',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataCategrayfile() {
		var url = url_formActionCategrayfile + '?operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winCategrayfile',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataCategrayfile() {
		var selectedArray = $(gridCategrayfile).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionCategrayfile + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winCategrayfile',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataCategrayfile() {
		var selectedArray = $(gridCategrayfile).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridCategrayfile).datagrid('loading');
					$.post(url_delActionCategrayfile + '?ids='
							+ $.farm.getCheckedIds(gridCategrayfile, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridCategrayfile).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridCategrayfile).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>