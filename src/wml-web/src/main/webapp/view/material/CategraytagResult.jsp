<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<!-- <div data-options="region:'north',border:false">
		<form id="searchCategraytagForm">
			<table class="editTable">
				<tr style="text-align: center;">
					<td colspan="4"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div> -->
	<div data-options="region:'center',border:false">
		<table id="dataCategraytagGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="CATEGRAYNAME" data-options="sortable:true" width="50">分类</th>
					<th field="TAGNAME" data-options="sortable:true" width="30">标签</th>
					<th field="CNUM" data-options="sortable:true" width="100">素材量</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var url_delActionCategraytag = "categraytag/del.do";//删除URL
	var url_formActionCategraytag = "categraytag/form.do";//增加、修改、查看URL
	var url_searchActionCategraytag = "categraytag/query.do?tagids=${tagids}";//查询URL
	var title_windowCategraytag = "素材分类标签管理";//功能名称
	var gridCategraytag;//数据表格对象
	var searchCategraytag;//条件查询组件对象
	var toolBarCategraytag = [
	//{
	//	id : 'view',
	//	text : '查看',
	//	iconCls : 'icon-tip',
	//	handler : viewDataCategraytag
	//}, 
	{
		id : 'add',
		text : '添加',
		iconCls : 'icon-add',
		handler : bindCategraytag
	},
	//{
	//	id : 'edit',
	//	text : '修改',
	//	iconCls : 'icon-edit',
	//	handler : editDataCategraytag
	//},
	{
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataCategraytag
	} ];
	$(function() {
		//初始化数据表格
		gridCategraytag = $('#dataCategraytagGrid').datagrid({
			url : url_searchActionCategraytag,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarCategraytag,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		//searchCategraytag = $('#searchCategraytagForm').searchForm({
		//	gridObj : gridCategraytag
		//});
	});
	//查看
	function viewDataCategraytag() {
		var selectedArray = $(gridCategraytag).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionCategraytag + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winCategraytag',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function bindCategraytag() {
		$.farm.openWindow({
			id : 'categrayTreeNodeWin',
			width : 250,
			height : 300,
			modal : true,
			url : "categray/treeNodeTreeView.do",
			title : '绑定选中分类'
		});
		chooseWindowCallBackHandle = function(node) {
			$.post('categraytag/dobind.do', {
				'tagids' : '${tagids}',
				'categrayid' : node.id
			}, function(flag) {
				if (flag.STATE == 0) {
					$(gridCategraytag).datagrid('reload');
					$('#categrayTreeNodeWin').window('close');
				} else {
					var str = MESSAGE_PLAT.ERROR_SUBMIT + flag.MESSAGE;
					$.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
				}
			}, 'json');
		};
	}
	//新增
	function addDataCategraytag() {
		var url = url_formActionCategraytag + '?operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winCategraytag',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataCategraytag() {
		var selectedArray = $(gridCategraytag).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionCategraytag + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winCategraytag',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataCategraytag() {
		var selectedArray = $(gridCategraytag).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridCategraytag).datagrid('loading');
					$.post(url_delActionCategraytag + '?ids='
							+ $.farm.getCheckedIds(gridCategraytag, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridCategraytag).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridCategraytag).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>