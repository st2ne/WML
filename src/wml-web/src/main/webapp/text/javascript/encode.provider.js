var AuthKeyProvider = {
	encodeLogin : function(type, password, loginname, salt) {
		if (type == 'SIMPLE') {
			return Base64.encode(password);
		}
		if (type == 'SAFE') {
			return hex_md5(password + "FARM");
		}
		if (type == 'SAFE2') {
			if (!salt) {
				alert("salt is null!type:"+type+",password:"+password+",loginname:"+loginname+",salt:"+salt);
			}
			loginname = loginname.trim();
			password = password.trim();
			var pripassword = hex_md5(hex_md5(password + "FARM").toUpperCase()
					+ loginname
					+ "MACPROW234CPAUTOGEN23657984ERATE5234WERTWERTDME234THODSTUB");
			var hashc = SHA256_hash(pripassword.toUpperCase() + salt);
			return hashc;
		}
		alert("not find the type " + type
				+ " by AuthKeyProvider(encode.provider.js)");
		return password;
	},
	encodeEdit : function(type, password, loginname, salt) {
		if (type == 'SIMPLE') {
			return Base64.encode(password);
		}
		if (type == 'SAFE') {
			return hex_md5(password + "FARM");
		}
		if (type == 'SAFE2') {
			loginname = loginname.trim();
			password = password.trim();
			var pripassword = hex_md5(hex_md5(password + "FARM").toUpperCase()
					+ loginname
					+ "MACPROW234CPAUTOGEN23657984ERATE5234WERTWERTDME234THODSTUB");
			return pripassword;
		}
		alert("not find the type " + type
				+ " by AuthKeyProvider(encode.provider.js)");
		return password;
	}
};