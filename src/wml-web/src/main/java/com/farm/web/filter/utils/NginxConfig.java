package com.farm.web.filter.utils;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

/**
 * 应用参数工具类，获得config中定义的参数
 * 
 * @author wangdong
 * 
 */
public class NginxConfig {
	private static final String BUNDLE_NAME = "config/nginx"; //$NON-NLS-1$
	private static final Logger log = Logger.getLogger(NginxConfig.class);
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

	private NginxConfig() {
	}

	public static String getString(String key, String servername) {
		try {
			if (RESOURCE_BUNDLE.containsKey(servername + "." + key)) {
				String messager = RESOURCE_BUNDLE.getString(servername + "." + key);
				return messager;
			} else {
				String messager = RESOURCE_BUNDLE.getString(key);
				return messager;
			}
		} catch (MissingResourceException e) {
			String messager = "不能在配置文件" + BUNDLE_NAME + "中发现参数：" + '!' + key + '!';
			log.error(messager);
			return "NONE";
		}
	}
}
