package com.farm.wcp.util;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.farm.authority.FarmAuthorityService;
import com.farm.authority.password.PasswordProviderService;
import com.farm.core.AuthorityService;
import com.farm.core.auth.domain.LoginUser;
import com.farm.core.auth.exception.LoginUserNoAuditException;
import com.farm.core.auth.exception.LoginUserNoExistException;
import com.farm.parameter.FarmParameterService;


public class UserInfoForcedModifications {
	private static final Logger log = Logger.getLogger(UserInfoForcedModifications.class);
	
	public static boolean isForcedModificationPassword(LoginUser currentUser, HttpSession session)
			throws LoginUserNoExistException, LoginUserNoAuditException {
		AuthorityService authServer = FarmAuthorityService.getInstance();
		if (FarmParameterService.getInstance().getParameter("config.sys.enforce.password.update").equals("true")
				&& authServer.isLegality(currentUser.getLoginname(),
						PasswordProviderService.getInstanceProvider().getClientPassword(currentUser.getLoginname(),
								FarmParameterService.getInstance().getParameter("config.default.password")),
						(String) session.getAttribute("saltkey"))) {
			// 用户未修改密码
			log.info("配置文件要求 ：用户必须修改密码");
			return true;
		}
		return false;
	}

	
	public static String getPasswordUpdateUrl() {
		return "userspace/settingpsd.do";
	}

}
