package com.farm.file.service.impl;

import com.farm.file.domain.FileBase;
import com.farm.file.domain.FileResource;
import com.farm.file.domain.FileText;
import com.farm.file.domain.Visit;
import com.farm.file.domain.ex.FileView;
import com.farm.file.enums.FileModel;
import com.farm.file.exception.FileExNameException;
import com.farm.core.time.TimeTool;

import org.apache.commons.io.output.FileWriterWithEncoding;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.farm.file.dao.FileBaseDaoInter;
import com.farm.file.dao.VisitlogsDaoInter;
import com.farm.file.service.FileBaseServiceInter;
import com.farm.file.service.FileResourceServiceInter;
import com.farm.file.service.FileTextServiceInter;
import com.farm.file.service.VisitServiceInter;
import com.farm.file.util.FarmDocFiles;
import com.farm.file.util.FileCopyProcessCache;
import com.farm.file.util.FileDirDeleteUtils;
import com.farm.file.util.FileMd5EncodeUtils;
import com.farm.file.util.ViewDirUtils;
import com.farm.util.web.FarmFormatUnits;
import com.farm.util.web.FarmHtmlUtils;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DBSort;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;
import com.farm.core.config.AppConfig;


@Service
public class FileBaseServiceImpl implements FileBaseServiceInter {
	@Resource
	private FileBaseDaoInter filebaseDaoImpl;
	@Resource
	private VisitServiceInter fileVisitServiceImpl;
	@Resource
	private FileTextServiceInter fileTextServiceImpl;
	@Resource
	private FileResourceServiceInter fileResourceServiceImpl;
	@Resource
	private VisitlogsDaoInter visitlogsDaoImpl;
	private static final Logger log = Logger.getLogger(FileBaseServiceImpl.class);

	@Override
	@Transactional
	public FileBase insertFilebaseEntity(FileBase entity, LoginUser user) {
		entity.setCuser(user.getId());
		entity.setCtime(TimeTool.getTimeDate14());
		entity.setCusername(user.getName());
		entity.setEuser(user.getId());
		entity.setEusername(user.getName());
		entity.setEtime(TimeTool.getTimeDate14());
		if (entity.getPstate() == null) {
			entity.setPstate("0");
		}
		if (entity.getFilesize() == null) {
			entity.setFilesize(0);
		}
		String id = UUID.randomUUID().toString().replaceAll("-", "").replaceAll(" ", "");
		entity.setSecret(id);
		entity = filebaseDaoImpl.insertEntity(entity);
		fileVisitServiceImpl.insertFilevisitEntity(entity, user);
		fileTextServiceImpl.insertFiletextEntity(entity, user);
		return entity;
	}

	@Override
	@Transactional
	public FileBase editFilebaseEntity(FileBase entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		FileBase entity2 = filebaseDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setResourceid(entity.getResourceid());
		entity2.setSysname(entity.getSysname());
		entity2.setPstate(entity.getPstate());
		entity2.setExname(entity.getExname());
		entity2.setRelativepath(entity.getRelativepath());
		// entity2.setSecret(entity.getSecret());
		entity2.setFilename(entity.getFilename());
		entity2.setTitle(entity.getTitle());
		// entity2.setFilesize(entity.getFilesize());
		entity2.setPcontent(entity.getPcontent());
		// entity2.setEuser(entity.getEuser());
		// entity2.setEusername(entity.getEusername());
		// entity2.setCuser(entity.getCuser());
		// entity2.setCusername(entity.getCusername());
		// entity2.setEtime(entity.getEtime());
		// entity2.setCtime(entity.getCtime());
		entity2.setId(entity.getId());
		filebaseDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteFilebaseEntity(String id, LoginUser user) {
		if(AppConfig.getBoolean("config.sys.demo")) {
			throw new RuntimeException("演示环境禁止物理删除文件");
		}
		FileBase filebase = filebaseDaoImpl.getEntity(id);
		if (filebase.getPstate().equals("2") || filebase.getPstate().equals("1")) {
			throw new RuntimeException("the file can't delete!state is " + filebase.getPstate());
		}
		// 删除附件统计表
		fileVisitServiceImpl.deleteVisitByAppid(id, user);
		// 删除文件文本表
		fileTextServiceImpl.deleteFiletextByFileid(id, user);
		// 刪除文件本表
		filebaseDaoImpl.deleteEntity(filebase);
		// 刪除統計日志
		visitlogsDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("APPID", id, "=")).toList());
		{
			// 物理删除文件
			File file = getFile(filebase);
			if (file.exists()) {
				if (!file.delete()) {
					System.gc();
					throw new RuntimeException("the file not delete !" + file.getPath());
				}
			}
			File viewDir = new File(ViewDirUtils.getPath(file));
			FileDirDeleteUtils.delFolder(viewDir.getPath());
		}
	}

	@Override
	@Transactional
	public FileBase getFilebaseEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return filebaseDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createFilebaseSimpleQuery(DataQuery query) {
		DataQuery dbQuery = DataQuery.init(query, "WML_F_FILE",
				"ID,RESOURCEID,SYSNAME,PSTATE,EXNAME,RELATIVEPATH,SECRET,FILENAME,TITLE,FILESIZE,PCONTENT,EUSER,EUSERNAME,CUSER,CUSERNAME,ETIME,CTIME");
		dbQuery.setDefaultSort(new DBSort("CTIME", "desc"));
		return dbQuery;
	}

	@Override
	@Transactional
	public void deleteFileByLogic(String fileid, LoginUser currentUser) {
		FileBase entity = filebaseDaoImpl.getEntity(fileid);
		entity.setPstate("9");
		filebaseDaoImpl.editEntity(entity);
	}

	@Override
	public boolean validateFileSize(long filesize, FileModel model) {
		if (filesize > Long.valueOf(model.getMaxsize())) {
			return false;
		}
		return true;
	}

	@Override
	public boolean validateFileExname(String originalFilename, FileModel model) {
		String exname = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
		if (!model.getExnames().contains(exname.toUpperCase()) && !model.getExnames().contains(exname.toLowerCase())) {
			return false;
		}
		return true;
	}

	@Override
	@Transactional
	public FileBase saveFile(File file, String filename, LoginUser currentUser, String fileProcesskey) {
		FileBase filebase = initFileBase(currentUser, filename, file.length(), null, "本地");
		FarmDocFiles.copyFile(file, getDirRealPath(filebase.getId()), filebase.getFilename(), fileProcesskey);
		return filebase;
	}

	@Override
	@Transactional
	public FileBase saveFile(byte[] data, String filename, LoginUser currentUser) {
		FileBase filebase = initFileBase(currentUser, filename, data.length, null, "本地");
		FarmDocFiles.saveFile(data, filebase.getFilename(), getDirRealPath(filebase.getId()));
		return filebase;
	}

	@Override
	@Transactional
	public FileBase saveFile(final InputStream inputStream, final String filename, LoginUser currentUser,
			final String fileProcesskey, final Integer filelength) {
		final FileBase filebase = initFileBase(currentUser, filename, filelength, null, "本地");
		final String fname = filebase.getFilename();
		final String realPath = getDirRealPath(filebase.getId());
		if (StringUtils.isNotBlank(fileProcesskey)) {
			FileCopyProcessCache.setProcess(fileProcesskey, 1);
		}
		Thread t = new Thread(new Runnable() {
			public void run() {
				// 启动线程异步下载
				FarmDocFiles.saveFile(inputStream, fname, realPath, fileProcesskey, filelength);
			}
		});
		t.start();
		return filebase;
	}

	@Override
	@Transactional
	public FileBase initFileBase(LoginUser currentUser, String filename, long filesize, String appid, String sysName) {
		if (StringUtils.isNotBlank(appid)) {
			String fileid = getFileIdByAppId(appid);
			if (fileid != null) {
				return getFilebaseEntity(fileid);
			}
		}
		FileBase entity = new FileBase();
		String exName = FarmDocFiles.getExName(filename);
		entity.setFilename(UUID.randomUUID().toString().replaceAll("-", "") + "." + exName + ".file");
		entity.setTitle(filename);
		if (StringUtils.isNotBlank(appid)) {
			entity.setAppid(appid);
		}
		entity.setSysname(sysName);
		entity.setExname(exName);
		entity.setFilesize(new Long(filesize).intValue());
		entity.setRelativepath(FarmDocFiles.generateDir());
		entity.setResourceid(fileResourceServiceImpl.getRandomResourceid());
		return insertFilebaseEntity(entity, currentUser);
	}

	@Override
	@Transactional
	public File getFile(FileBase filebase) {
		return new File(getDirRealPath(filebase) + filebase.getFilename());
	}

	@Override
	@Transactional
	public String getDirRealPath(FileBase filebase) {
		FileResource resource = fileResourceServiceImpl.getFileresourceEntity(filebase.getResourceid());
		String path = resource.getPath() + filebase.getRelativepath();
		File dir = new File(path);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		return path;
	}

	@Override
	@Transactional
	public String getDirRealPath(String fileid) {
		FileBase file = filebaseDaoImpl.getEntity(fileid);
		return getDirRealPath(file);
	}

	@Override
	@Transactional
	public String getFileRealPath(String fileid) {
		return getDirRealPath(fileid) + getFilebaseEntity(fileid).getFilename();
	}

	@Override
	@Transactional
	public List<FileView> getTempFiles(int cpage, int size) {
		DataQuery query = DataQuery.getInstance(cpage,
				"ID,RESOURCEID,SYSNAME,PSTATE,EXNAME,RELATIVEPATH,SECRET,FILENAME,TITLE,FILESIZE,PCONTENT,EUSER,EUSERNAME,CUSER,CUSERNAME,ETIME,CTIME",
				"WML_F_FILE");
		query.addRule(new DBRule("PSTATE", "0", "="));
		query.addSort(new DBSort("CTIME", "DESC"));
		query.setPagesize(size);
		query.setCurrentPage(cpage);
		List<FileView> returnObj = new ArrayList<FileView>();
		try {
			DataResult result = query.search();
			List<FileBase> filebase = result.getObjectList(FileBase.class);
			for (FileBase node : filebase) {
				FileView view = new FileView();
				view.setFile(node);
				try {
					view.setDownloadUrl(getDownloadUrl(node.getId(), FileModel.getModelByFileExName(node.getExname())));
					view.setIconUrl(getIconUrl(node.getId(), FileModel.getModelByFileExName(node.getExname())));
				} catch (Exception e) {
					log.error(e);
				}
				returnObj.add(view);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return returnObj;
	}

	@Override
	@Transactional
	public String getDownloadUrl(String fileid, FileModel model) {
		FileBase filebase = getFilebaseEntity(fileid);
		return "download/Pubfile.do?id=" + fileid + "&secret=" + filebase.getSecret();
	}

	@Override
	@Transactional
	public String getPlayUrl(String fileid, FileModel model) {
		FileBase filebase = getFilebaseEntity(fileid);
		return "download/Pubload.do?id=" + fileid + "&secret=" + filebase.getSecret();
	}

	@Override
	public String getIconUrl(String fileid, FileModel model) {
		return "download/Pubicon.do?id=" + fileid;
	}

	@Override
	@Transactional
	public String submitFile(String fileid) throws FileNotFoundException {
		FileBase entity = filebaseDaoImpl.getEntity(fileid);
		File file = new File(getFileRealPath(fileid));
		String secret = entity.getSecret();
		if (!file.exists()) {
			throw new FileNotFoundException("the file is not exist:" + file.getPath());
		}
		if (file.length() < 200 * 1024 * 1024) {
			try {
				log.info("file size:" + file.length() + "run md5 encodeing.....");
				entity.setSecret(FileMd5EncodeUtils.calcMD5(file));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		entity.setPstate("1");
		entity.setFilesize(new Long(file.length()).intValue());
		filebaseDaoImpl.editEntity(entity);
		return secret + ":" + entity.getSecret();
	}

	@Override
	@Transactional
	public List<FileView> getPresistFiles(int cpage, int size) {
		DataQuery query = DataQuery.getInstance(cpage,
				"ID,RESOURCEID,SYSNAME,PSTATE,EXNAME,RELATIVEPATH,SECRET,FILENAME,TITLE,FILESIZE,PCONTENT,EUSER,EUSERNAME,CUSER,CUSERNAME,ETIME,CTIME",
				"WML_F_FILE");
		query.addSqlRule("and (PSTATE='1' or PSTATE='2') ");
		query.addSort(new DBSort("CTIME", "DESC"));
		query.setPagesize(size);
		query.setCurrentPage(cpage);
		List<FileView> returnObj = new ArrayList<FileView>();
		try {
			DataResult result = query.search();
			List<FileBase> filebase = result.getObjectList(FileBase.class);
			for (FileBase node : filebase) {
				FileView view = new FileView();
				view.setFile(node);
				try {
					view.setDownloadUrl(getDownloadUrl(node.getId(), FileModel.getModelByFileExName(node.getExname())));
					view.setIconUrl(getIconUrl(node.getId(), FileModel.getModelByFileExName(node.getExname())));
				} catch (Exception e) {
					log.error(e);
				}
				returnObj.add(view);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return returnObj;
	}

	@Override
	@Transactional
	public FileView getFileView(String fileid) throws FileNotFoundException {
		FileView view = new FileView();
		FileBase filebase = getFilebaseEntity(fileid);
		if (filebase == null) {
			throw new FileNotFoundException("the fileid is not exist database!");
		}
		view.setFile(filebase);
		try {
			view.setDownloadUrl(getDownloadUrl(fileid, FileModel.getModelByFileExName(view.getFile().getExname())));
			view.setIconUrl(getIconUrl(fileid, FileModel.getModelByFileExName(view.getFile().getExname())));
			view.setModellabel(FileModel.getModelByFileExName(view.getFile().getExname()).name());
		} catch (Exception e) {
			log.error(e);
		}
		Visit visit = fileVisitServiceImpl.getVisitByAppId(fileid);
		Map<String, String> stateMap = new HashMap<>();
		// 0.临时1.持久2永久9删除
		stateMap.put("0", "临时");
		stateMap.put("1", "持久");
		stateMap.put("2", "永久");
		stateMap.put("9", "删除");
		view.setVisit(fileVisitServiceImpl.getVisitByAppId(fileid));
		view.setStatelabel(stateMap.get(view.getFile().getPstate()));
		view.setResource(fileResourceServiceImpl.getFileresourceEntity(view.getFile().getResourceid()));
		view.setRealpath(getFileRealPath(fileid));
		view.setPhysicsExist(new File(getFileRealPath(fileid)).exists());
		view.setSizelabel(FarmFormatUnits.getFileLengthAndUnit(filebase.getFilesize()));
		view.setViewNum(visit.getViewnum());
		view.setDownum(visit.getDownum());
		return view;
	}

	@Override
	@Transactional
	public void freeFile(String fileid) {
		FileBase entity = filebaseDaoImpl.getEntity(fileid);
		if (entity == null) {
			throw new RuntimeException("the file is not exist:" + fileid);
		}
		entity.setPstate("0");
		filebaseDaoImpl.editEntity(entity);
	}

	@Override
	@Transactional
	public String getFileIdByAppId(String appid) {
		if (StringUtils.isBlank(appid)) {
			return null;
		}
		List<FileBase> files = filebaseDaoImpl.selectEntitys(DBRuleList.getInstance()
				.add(new DBRule("APPID", appid, "=")).add(new DBRule("PSTATE", "9", "!=")).toList());
		if (files.size() > 0) {
			return files.get(0).getId();
		} else {
			return null;
		}
	}

	@Override
	@Transactional
	public void editFileSize(String fileId, int length) {
		FileBase entity = filebaseDaoImpl.getEntity(fileId);
		entity.setFilesize(length);
		filebaseDaoImpl.editEntity(entity);
	}

	@Override
	@Transactional
	public void WriteTextFile(String fileId, String text) {
		if (StringUtils.isBlank(fileId) || StringUtils.isBlank(text)) {
			return;
		}
		File file = getFile(getFilebaseEntity(fileId));
		FileWriterWithEncoding writer = null;
		try {
			writer = new FileWriterWithEncoding(file, "utf-8");
			writer.write(text);
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		FileText filetext = fileTextServiceImpl.getFiletextByFileId(fileId);
		filetext.setFiletext(FarmHtmlUtils.HtmlRemoveTag(text));
		fileTextServiceImpl.editFiletextEntity(filetext);
	}

}
