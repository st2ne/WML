package com.farm.file.service;

import com.farm.file.domain.FileResource;
import com.farm.core.sql.query.DataQuery;

import java.util.List;

import com.farm.core.auth.domain.LoginUser;


public interface FileResourceServiceInter {
	
	public FileResource insertFileresourceEntity(FileResource entity, LoginUser user);

	
	public FileResource editFileresourceEntity(FileResource entity, LoginUser user);

	
	public void deleteFileresourceEntity(String id, LoginUser user);

	
	public FileResource getFileresourceEntity(String id);

	
	public DataQuery createFileresourceSimpleQuery(DataQuery query);

	
	public List<FileResource> getResources(boolean loadSpaceInfo);

	
	public String getRandomResourceid();
}