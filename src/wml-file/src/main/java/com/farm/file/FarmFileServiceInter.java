package com.farm.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.farm.core.auth.domain.LoginUser;
import com.farm.file.domain.FileBase;
import com.farm.file.domain.ex.FileView;
import com.farm.file.domain.ex.PersistFile;
import com.farm.file.enums.FileModel;
import com.farm.file.exception.FileExNameException;
import com.farm.file.exception.FileSizeException;


public interface FarmFileServiceInter {

	
	public FileBase saveLocalFile(MultipartFile file, FileModel model, LoginUser currentUser, String fileProcesskey)
			throws FileSizeException, FileExNameException;

	public FileBase saveLocalFile(File file, FileModel model, LoginUser currentUser, String fileProcesskey)
			throws FileSizeException, FileExNameException;

	
	public FileBase saveLocalFile(byte[] data, FileModel fileModel, String filename, LoginUser currentUser)
			throws FileSizeException, FileExNameException;

	
	public String getDownloadUrl(String fileid, FileModel model);

	
	public String getPlayUrl(String fileid, FileModel fileModel);

	
	public String getExName(String filename);

	
	public PersistFile getFileIconFile(String fileid);

	
	public void delFileByLogic(String fileid);

	
	public String submitFile(String fileid) throws FileNotFoundException;

	
	public void freeFile(String fileid);

	
	public PersistFile getPersistFile(String fileid);

	
	public String getFileRealPath(String fileid);

	
	public FileBase initFile(LoginUser user, String filename, long length, String appid, String sysname);

	
	public String getIconUrl(String fileid);

	
	public FileModel getFileModel(String fileid) throws FileExNameException;

	
	public FileView getFileInfo(String fileid) throws FileNotFoundException;

	
	public String getFiletext(String fileid);

	
	public String getFileIdByAppId(String appid);

	
	public String readFileToText(String fileid);

	
	public void updateFilesize(String fileId, int length);

	
	public void writeFileByText(String fileId, String text);

	
	public FileBase getFileBase(String fileid);

	
	public FileBase saveRemoteUrlFile(String fileDownloadUrl, String filename, FileModel fileModel,
			LoginUser currentUser, String fileProcesskey) throws FileSizeException, FileExNameException, IOException;

}
