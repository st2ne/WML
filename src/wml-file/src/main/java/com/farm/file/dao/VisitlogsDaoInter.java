package com.farm.file.dao;

import com.farm.file.domain.Visitlogs;
import org.hibernate.Session;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import java.util.List;
import java.util.Map;


public interface VisitlogsDaoInter {
	
	public void deleteEntity(Visitlogs visitlogs);

	
	public Visitlogs getEntity(String visitlogsid);

	
	public Visitlogs insertEntity(Visitlogs visitlogs);

	
	public int getAllListNum();

	
	public void editEntity(Visitlogs visitlogs);

	
	public Session getSession();

	
	public DataResult runSqlQuery(DataQuery query);

	
	public void deleteEntitys(List<DBRule> rules);

	
	public List<Visitlogs> selectEntitys(List<DBRule> rules);

	
	public void updataEntitys(Map<String, Object> values, List<DBRule> rules);

	
	public int countEntitys(List<DBRule> rules);
	public int sumEntitys(List<DBRule> rules);
}