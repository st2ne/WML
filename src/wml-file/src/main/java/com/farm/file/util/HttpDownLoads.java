package com.farm.file.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.lang.StringUtils;

public class HttpDownLoads {

	
	public static File downloadWebFile(String eurl, String filename, File file) throws IOException {
		String exname = null;
		try {
			if (eurl.lastIndexOf("?") > 0) {
				exname = eurl.substring(0, eurl.lastIndexOf("?"));
			} else {
				exname = eurl;
			}
			if (exname.lastIndexOf(".") > 0) {
				exname = exname.substring(exname.lastIndexOf(".") + 1);
			} else {
				exname = eurl;
			}
			if (exname == null || exname.length() > 10) {
				exname = "gif";
			}
		} catch (Exception e) {
			exname = "gif";
		}
		try {
			validateUrl(eurl);
			URL innerurl = new URL(eurl);
			// 创建连接的地址
			HttpURLConnection connection = (HttpURLConnection) innerurl.openConnection();
			// 返回Http的响应状态码
			InputStream input = null;
			try {
				input = connection.getInputStream();
			} catch (Exception e) {
				throw new RuntimeException("文件无法访问：" + eurl);
			}
			file.getParentFile().mkdirs();
			OutputStream fos = new FileOutputStream(file);
			// 获取输入流
			try {
				int bytesRead = 0;
				byte[] buffer = new byte[8192];
				while ((bytesRead = input.read(buffer, 0, 8192)) != -1) {
					fos.write(buffer, 0, bytesRead);
				}
			} finally {
				input.close();
				fos.close();
			}
			return file;
		} catch (IOException e) {
			e = new IOException(e);
			throw e;
		}
	}

	
	public static InputStream downloadWebFile(String eurl) throws IOException {
		String exname = null;
		try {
			if (eurl.lastIndexOf("?") > 0) {
				exname = eurl.substring(0, eurl.lastIndexOf("?"));
			} else {
				exname = eurl;
			}
			if (exname.lastIndexOf(".") > 0) {
				exname = exname.substring(exname.lastIndexOf(".") + 1);
			} else {
				exname = eurl;
			}
			if (exname == null || exname.length() > 10) {
				exname = "gif";
			}
		} catch (Exception e) {
			exname = "gif";
		}
		try {
			validateUrl(eurl);
			URL innerurl = new URL(eurl);
			// 创建连接的地址
			HttpURLConnection connection = (HttpURLConnection) innerurl.openConnection();
			connection.setConnectTimeout(3000);
			connection.setReadTimeout(3000);
			// 返回Http的响应状态码
			InputStream input = null;
			try {
				input = connection.getInputStream();
			} catch (Exception e) {
				throw new RuntimeException("文件无法访问：" + eurl);
			}
			return input;
		} catch (IOException e) {
			e = new IOException(e);
			throw e;
		}
	}

	
	private static void validateUrl(String url) {
		// 处理和校验URL
		if (StringUtils.isNotBlank(url)) {
			int num = url.indexOf("://");
			if (num > 0) {
				String protocol = url.substring(0, num);
				if (!protocol.trim().toUpperCase().equals("HTTP") && !protocol.trim().toUpperCase().equals("HTTPS")) {
					throw new RuntimeException("can't do the protocol:" + protocol);
				}
			}
		}
	}

	public static int getLength(String eurl) {
		URL innerurl;
		HttpURLConnection connection = null;
		try {
			innerurl = new URL(eurl);
			connection = (HttpURLConnection) innerurl.openConnection();
			connection.setConnectTimeout(3000);
			connection.setReadTimeout(3000);
			connection.getHeaderFields();
			int size = connection.getContentLength();
			return size;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return 0;
		} catch (IOException e) {
			e.printStackTrace();
			return 0;
		}
	}

	public static void main(String[] args) {
		int filesize = getLength("http://jxzx.bit.edu.cn/docs/2020-10/20201020031513821052.pptx");
		System.out.println(filesize / 1024);
	}
}
