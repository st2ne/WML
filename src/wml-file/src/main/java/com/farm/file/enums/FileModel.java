package com.farm.file.enums;

import java.util.List;

import com.farm.file.exception.FileExNameException;
import com.farm.parameter.FarmParameterService;


public enum FileModel {
	IMG, MEDIA, OFFICE, FILE;

	
	public long getMaxsize() {
		if (this.equals(IMG)) {
			return FarmParameterService.getInstance().getParameterLong("config.doc.img.upload.length.max");
		}
		return FarmParameterService.getInstance().getParameterLong("config.doc.upload.length.max");
	}

	public List<String> getExnames() {
		if (this.equals(IMG)) {
			return FarmParameterService.getInstance().getParameterStringList("config.doc.img.upload.types");
		}
		if (this.equals(MEDIA)) {
			return FarmParameterService.getInstance().getParameterStringList("config.doc.media.upload.types");
		}
		return FarmParameterService.getInstance().getParameterStringList("config.doc.upload.types");
	}

	public static FileModel getModel(String type) {
		if (IMG.name().equals(type.toUpperCase())) {
			return IMG;
		}
		if (MEDIA.name().equals(type.toUpperCase())) {
			return MEDIA;
		}
		return FILE;
	}

	
	public static FileModel getModelByFileExName(String exName) {
		for (FileModel model : values()) {
			if (model.getExnames().contains(exName.toUpperCase())
					|| model.getExnames().contains(exName.toLowerCase())) {
				return model;
			}
		}
		return FILE;
	}
}
