package com.farm.file.service;

import com.farm.file.domain.FileBase;
import com.farm.file.domain.FileText;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.auth.domain.LoginUser;


public interface FileTextServiceInter {
	
	public FileText insertFiletextEntity(FileText entity, LoginUser user);

	
	public FileText editFiletextEntity(FileText entity, LoginUser user);

	public FileText editFiletextEntity(FileText filetext);
	
	public void deleteFiletextEntity(String id, LoginUser user);

	
	public FileText getFiletextEntity(String id);

	
	public DataQuery createFiletextSimpleQuery(DataQuery query);

	
	public void insertFiletextEntity(FileBase entity, LoginUser user);

	
	public FileText getFiletextByFileId(String fileid);

	
	public void deleteFiletextByFileid(String fileid, LoginUser user);

	
	public void clearFiletextByFileid(String fileid, LoginUser loginUser);

}